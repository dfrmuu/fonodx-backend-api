import { Router } from "express"
import { validacionBusquedaPaciente,
         validacionFiltrarPacientes,
         validacionMisPacientes,
         validacionPaciente, 
         validacionUsuarioPaciente,
         validarEditarPaciente,
         validarEnviarResultados,
         validarTraerPaciente
} from "../business/patient.business.js"

import { traerPacientes, 
        crearPaciente, 
        buscarPaciente,
        crearUsuarioPaciente,
        misPacientes,
        filtrarPacientes,
        editarPaciente,
        enviarResultados
} from "../controllers/patient.controller.js"

import { revisarCliente } from '../middleware/client.middleware.js'
import { revisarToken } from "../middleware/token.middleware.js"
import { validarPermiso } from "../middleware/privilege.middleware.js"


/*
 * Proyecto: FonoDX
 * Archivo de rutas: Pacientes
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de pacientes.
 */

const router = Router()

router.get("/paciente/mis-pacientes", revisarToken, revisarCliente, validarPermiso,validacionMisPacientes ,misPacientes)
router.get("/paciente/traer/:id", revisarToken, revisarCliente, validarPermiso,validarTraerPaciente ,traerPacientes)
router.post("/paciente/crear", revisarToken, revisarCliente, validarPermiso, validacionPaciente ,crearPaciente)
router.put("/paciente/editar", revisarToken, revisarCliente, validarPermiso, validarEditarPaciente ,editarPaciente)
router.post("/paciente/registrar", revisarToken, revisarCliente, validarPermiso, validacionUsuarioPaciente ,crearUsuarioPaciente)
router.post("/paciente/buscar",revisarToken,revisarCliente, validarPermiso, validacionBusquedaPaciente, buscarPaciente)
router.post("/paciente/filtrar", revisarToken, revisarCliente, validarPermiso, validacionFiltrarPacientes, filtrarPacientes)
router.post("/paciente/enviar/resultados", revisarToken, revisarCliente, validarPermiso, validarEnviarResultados, enviarResultados )

export default router