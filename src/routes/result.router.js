import { Router } from "express";
import { validarPermiso } from "../middleware/privilege.middleware.js";
import { revisarToken } from "../middleware/token.middleware.js";
import { 
    validarResultadosSenal 
} from "../business/results.business.js";
import { 
    traerResultadoSenal 
} from "../controllers/results.controller.js";

/*
 * Proyecto: FonoDX
 * Archivo de rutas: Resultados
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de resultados.
 */


const router = Router()

router.get("/resultados/senal/:id", revisarToken, validarResultadosSenal, traerResultadoSenal) //Agregar permisos


export default router

