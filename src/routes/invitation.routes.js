import { Router } from "express"
import { 
    validacionEditarInvitacion,
    validacionInvitacion,
    validacionMisInvitaciones, 
    validarEliminarInvitacion, 
    validarFiltrarDatos, 
    validarTraerInvitacion
} from "../business/invitation.business.js"

import {
    crearInvitacion,
    editarInvitacion,
    eliminarInvitacion,
    filtrarInvitaciones,
    misInvitaciones,
    traerInvitacion
} from '../controllers/invitation.controller.js'

import { revisarToken } from "../middleware/token.middleware.js"
import { validarPermiso } from "../middleware/privilege.middleware.js"


/*
 * Proyecto: FonoDX
 * Archivo de rutas: Invitaciones
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de invitaciones.
 */


const router = Router()

router.get("/invitaciones/mis-invitaciones",revisarToken,validarPermiso,validacionMisInvitaciones, misInvitaciones)
router.get("/invitaciones/traer/:id", revisarToken, validarPermiso, validarTraerInvitacion, traerInvitacion)
router.post("/invitaciones/enviar", revisarToken,validarPermiso,validacionInvitacion, crearInvitacion)
router.post("/invitaciones/filtrar", revisarToken, validarPermiso, validarFiltrarDatos, filtrarInvitaciones)
router.put("/invitaciones/editar", revisarToken,validarPermiso,validacionEditarInvitacion, editarInvitacion)
router.delete("/invitaciones/eliminar/:id", revisarToken, validarPermiso, validarEliminarInvitacion, eliminarInvitacion)

export default router