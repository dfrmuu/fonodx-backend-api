import { Router } from "express"
import {
    validacionActividades,
    validacionCrearActividad
} from '../business/activity.business.js'

import {
    actividadUsuario,
    guardarActividad,
} from '../controllers/activity.controller.js'

import { revisarToken } from "../middleware/token.middleware.js"


/*
 * Proyecto: FonoDX
 * Archivo de rutas: Actividades
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso actividades.
 */


const router = Router()

router.get('/actividades/mis-actividades', revisarToken,validacionActividades, actividadUsuario)
router.post('/actividad/crear', revisarToken,validacionCrearActividad,guardarActividad)

export default router