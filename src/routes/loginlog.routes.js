import { Router } from "express";
import {
    validarTraerLoginLogs,
    validarFiltrarLoginLogs
} from '../business/loginlog.business.js'
import {
    traerLoginLogs,
    filtrarLoginLogs
} from '../controllers/loginlog.controller.js'
import { revisarToken } from '../middleware/token.middleware.js'
import { validarPermiso } from '../middleware/privilege.middleware.js'

/*
 * Proyecto: FonoDX
 * Archivo de rutas: Logs inicio de sesión
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso logs de inicios de sesión.
 */


const router = Router()

router.get("/logs/iniciosesion/:id", revisarToken, validarTraerLoginLogs, traerLoginLogs)
router.post("/logs/iniciosesion/filtrar", revisarToken, validarFiltrarLoginLogs, filtrarLoginLogs)


export default router