import { Router } from "express";
import { 
    validarFiltrarDatos,
    validarTraerLogsErrorGeneral,
    validarTraerRecursos
} from "../business/errorlog.business.js";
import { 
    filtrarErrorLogs,
    traerLogsErrorGeneral, 
    traerRecursos
} from "../controllers/errorlog.controller.js";
import { revisarToken } from '../middleware/token.middleware.js'
import { revisarCliente } from '../middleware/client.middleware.js'
import { validarPermiso } from "../middleware/privilege.middleware.js";


/*
 * Proyecto: FonoDX
 * Archivo de rutas: Registros de error
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de logs de error.
 */


const router = Router()

router.get("/logs/errores", revisarToken, revisarCliente, validarPermiso, validarTraerLogsErrorGeneral, traerLogsErrorGeneral)
router.get("/logs/errores/recursos", revisarToken, revisarCliente, validarTraerRecursos, traerRecursos)
router.post("/logs/errores/filtrar", revisarToken, revisarCliente, validarFiltrarDatos, filtrarErrorLogs)

export default router