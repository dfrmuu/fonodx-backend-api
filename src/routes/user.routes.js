import Router from "express";
import {
    traerUsuarios,
    crearUsuario,
    editarUsuario,
    desactivarUsuario,
    activarUsuario,
    traerUsuario,
    eliminarUsuario,
    filtrarDatos,
    traerMiUsuario,
    editarMiUsuario,
    revisionClave,
    cambioClave,
} from "../controllers/user.controller.js"

import {
    validacionFiltrarUsuarios,
    validarActivarUsuario,
    validarCambioClave,
    validarCrearUsuario,
    validarDesactivarUsuario,
    validarEditarMiUsuario,
    validarEditarUsuario,
    validarEliminarUsuario,
    validarRevisionClave,
    validarTraerMiUsuario,
    validarTraerUsuario,
    validarTraerUsuarios
} from '../business/user.business.js'

import { revisarToken } from '../middleware/token.middleware.js'
import { validarPermiso } from "../middleware/privilege.middleware.js";

/*
 * Proyecto: FonoDX
 * Archivo de rutas: Usuarios
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de usuarios.
 */ 

const router = Router()

router.get("/usuarios",revisarToken, validarPermiso, validarTraerUsuarios,traerUsuarios)
router.get("/usuarios/:id", revisarToken,validarPermiso,  validarTraerUsuario,traerUsuario)
router.get("/usuarios/mi-usuario/info", revisarToken, validarPermiso, validarTraerMiUsuario, traerMiUsuario)
router.get("/usuarios/desactivar/:id", revisarToken, validarPermiso,  validarDesactivarUsuario, desactivarUsuario)
router.get("/usuarios/activar/:id", revisarToken, validarPermiso,  validarActivarUsuario, activarUsuario)
router.post("/usuarios/crear", revisarToken, validarPermiso, validarCrearUsuario, crearUsuario)
router.post("/usuarios/revisarclave", revisarToken, validarRevisionClave, revisionClave)
router.post("/usuarios/cambiarclave", revisarToken, validarCambioClave, cambioClave)
router.post("/usuarios/filtrar", revisarToken, validarPermiso, validacionFiltrarUsuarios, filtrarDatos)
router.put("/usuarios/editar", revisarToken, validarPermiso,  validarEditarUsuario, editarUsuario)
router.put("/usuarios/mi-usuario/editar", revisarToken, validarPermiso, validarEditarMiUsuario, editarMiUsuario)
router.delete("/usuarios/eliminar/:id", revisarToken, validarPermiso,  validarEliminarUsuario, eliminarUsuario)


export default router;