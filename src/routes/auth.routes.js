import { Router } from "express"

import { 
    validarCorreo, 
    validarGenerarTokenRecuperacion, 
    validarLogin, 
    validarRecuperarCredenciales, 
    validarRegistro, 
    validarToken
} from "../business/auth.business.js"

import {
    generarTokenRecuperacion,
    iniciarSesion,
    recuperarCredenciales,
    registroUsuario,
    revisarTokenRecuperacion,
    revisionCorreo,
    revisionUsuario,
} from '../controllers/auth.controller.js'

import { revisarToken } from "../middleware/token.middleware.js"
import { revisarCliente } from "../middleware/client.middleware.js"

/*
 * Proyecto: FonoDX
 * Archivo de rutas: Autentificación
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de autentificación.
 */

const router = Router()

router.get("/auth/revision", revisarToken, revisionUsuario)
router.post("/auth/login",revisarCliente , validarLogin, iniciarSesion)
router.post("/auth/revision-correo",revisarCliente , validarCorreo, revisionCorreo)
router.post("/auth/registro", revisarCliente, validarRegistro, registroUsuario)
router.post("/auth/recuperar", revisarCliente ,validarGenerarTokenRecuperacion, generarTokenRecuperacion)
router.post("/auth/validar-token",revisarCliente , validarToken, revisarTokenRecuperacion)
router.post("/auth/cambiarclave", revisarCliente, validarRecuperarCredenciales,recuperarCredenciales)

export default router;