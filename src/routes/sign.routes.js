import { Router } from "express"
import { validarEditarSenal, 
        validarEliminarSenal, 
        validarFiltrarAuscultaciones, 
        validarRecuperarFechasAuscultaciones, 
        validarRegistroSenal,
        validarSenales, 
        validarTraerSenal } 
from "../business/sign.business.js"
import { editarSenal, 
         eliminarSenal, 
         filtrarAuscultaciones, 
         recuperarFechasAuscultaciones, 
         registrarAuscultacion, 
         traerSenal, 
         traerSenales,
         traerSenalPorFechaID } 
from "../controllers/sign.controller.js"
import { revisarCliente } from '../middleware/client.middleware.js'
import { revisarToken } from "../middleware/token.middleware.js"
import { validarPermiso } from "../middleware/privilege.middleware.js"
import multer from "multer"
import { storage } from "../storage/storage.js"


/*
 * Proyecto: FonoDX
 * Archivo de rutas: Auscultaciones / señales
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de auscultaciones / señales.
 */


const router = Router()
const carga = multer({storage : storage})  //Este es una libreria que nos permite cargar archivos.

router.get("/senales/traer/:id", revisarToken,validarPermiso,validarTraerSenal, traerSenal)
router.post("/senales/fechas", revisarToken, validarPermiso, validarRecuperarFechasAuscultaciones, recuperarFechasAuscultaciones)
router.post("/senales/paciente", revisarToken,validarPermiso,validarSenales, traerSenales)
router.post("/senal/paciente/fecha", revisarToken,validarPermiso,validarSenales, traerSenalPorFechaID)
router.post("/senales/filtrar", revisarToken, validarPermiso, validarFiltrarAuscultaciones, filtrarAuscultaciones)
/* Carga.single('auscultacion') lo que hace es recuperar en los datos de la petición el archivo con el name / id de "auscultacion" para almacenarlo en el storage definido
   y poder usarlo más adelante en la petición.*/
router.post("/senal/registrar", revisarToken,validarPermiso,carga.single('auscultacion'),validarRegistroSenal, registrarAuscultacion)
router.put("/senal/editar", revisarToken,revisarCliente, validarEditarSenal, editarSenal)
router.delete("/senal/eliminar/:id", revisarToken, revisarCliente, validarEliminarSenal, eliminarSenal)

export default router