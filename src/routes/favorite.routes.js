import { Router } from "express"
import {
    validacionMisFavoritos,
    validacionFavorito,
    validacionEditarFavorito,
    validacionEliminarFavorito,
    validacionFiltrarFavoritos,
    validarRecuperarFechasFavoritos
} from '../business/favorite.business.js'

import {
    misFavoritos,
    crearFavorito,
    editarFavorito,
    eliminarFavorito,
    filtrarFavorito,
    recuperarFechasFavoritos
} from '../controllers/favorite.controller.js'

import {
    revisarToken
} from '../middleware/token.middleware.js'

/*
 * Proyecto: FonoDX
 * Archivo de rutas: Favoritos (señales de referencia)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de favoritos.
 */



const router = Router()

router.get("/favoritos/mis-favoritos",revisarToken,validacionMisFavoritos,misFavoritos)
router.get("/favoritos/fechas", revisarToken, validarRecuperarFechasFavoritos, recuperarFechasFavoritos)
router.post("/favoritos/crear",revisarToken,validacionFavorito,crearFavorito)
router.post("/favoritos/filtrar", revisarToken, validacionFiltrarFavoritos, filtrarFavorito)
router.put("/favoritos/editar",revisarToken,validacionEditarFavorito,editarFavorito)
router.delete('/favoritos/eliminar/:id',revisarToken,validacionEliminarFavorito, eliminarFavorito)

export default router