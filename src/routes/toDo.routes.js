import { Router } from "express"
import { revisarToken } from "../middleware/token.middleware.js"
import { 
    validarCompletar,
    validarCrearTareas, 
    validarEditarTareas, 
    validarEliminarTarea, 
    validarIncompleta, 
    validarTraerTarea, 
    validarTraerTareas 
} from "../business/toDo.business.js"
import {
    completarTarea,
    crearTarea, 
    editarTarea, 
    eliminarTarea, 
    tareaIncompleta, 
    traerTarea, 
    traerTareas 
} from "../controllers/toDo.controller.js"

/*
 * Proyecto: FonoDX
 * Archivo de rutas: Tareas
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de tareas.
 */

const router = Router()

router.get("/tareas/mis-tareas", revisarToken,validarTraerTareas, traerTareas)
router.get("/tareas/ver/:id", revisarToken, validarTraerTarea, traerTarea)
router.get("/tareas/incompleta/:id", revisarToken, validarIncompleta, tareaIncompleta)
router.get("/tareas/completa/:id", revisarToken, validarCompletar, completarTarea)
router.post("/tareas/crear", revisarToken, validarCrearTareas, crearTarea)
router.put("/tareas/editar", revisarToken, validarEditarTareas, editarTarea)
router.delete("/tareas/eliminar/:id", revisarToken, validarEliminarTarea, eliminarTarea)

export default router