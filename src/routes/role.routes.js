import { Router } from "express";
import { revisarToken } from '../middleware/token.middleware.js'
import { validarPermiso } from "../middleware/privilege.middleware.js"
import {
    validarAgregarPermiso,
    validarQuitarPermiso,
    validarTraerPermisos,
    validarTraerRoles
} from '../business/role.business.js'
import {
    agregarPermiso,
    quitarPermiso,
    traerPermisos,
    traerRoles
} from '../controllers/role.controller.js'

/*
 * Proyecto: FonoDX
 * Archivo de rutas: Roles
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Asignación de endpoints para el recurso de roles.
 */

const router = Router()

router.get("/roles",revisarToken,validarPermiso, validarTraerRoles,traerRoles)
router.get("/roles/permisos/:id", revisarToken,validarTraerPermisos, traerPermisos)
router.post("/roles/agregar/permiso",revisarToken,validarPermiso,validarAgregarPermiso,agregarPermiso)
router.post("/roles/quitar/permiso",revisarToken,validarPermiso,validarQuitarPermiso,quitarPermiso)
export default router