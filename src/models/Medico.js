import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : Medico (doctors)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 


export const Medico = sequelize.define("doctors", {
        id : {
            type : DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey : true,    
        },

        UserID : {
            type : DataTypes.UUID,
            allowNull : false
        },

        job_title : {
            type : DataTypes.STRING,
            allowNull : true
        },

        area : {
            type : DataTypes.STRING,
            allowNull : true
        },
        
        createdAt: {
            type : DataTypes.DATE,
            defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
            allowNull: true,
        },
},{
    timestamps: false
})



 

