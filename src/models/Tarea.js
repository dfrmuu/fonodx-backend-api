import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : Tarea (homeworks)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 

export const Tarea = sequelize.define('homeworks', {
    id  : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        allowNull : false,
        primaryKey : true
    },

    UserID : {
        type : DataTypes.UUID,
        allowNull : false,   
    },

    homework : {
        type : DataTypes.STRING,
        allowNull : false
    },

    description : {
        type : DataTypes.TEXT,
        allowNull : false
    },

    status : {
        type : DataTypes.BOOLEAN,
        allowNull : false
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },

},{
    timestamps : false
})