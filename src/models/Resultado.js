import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : Resultado (results)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 


export const Resultado = sequelize.define("results", {
    id : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        primaryKey : true,
    },

    SignalID : {
        type : DataTypes.UUID,
        allowNull : false
    },

    diagnostic : {
        type : DataTypes.FLOAT,
        allowNull: true,
    },

    disease : {
        type : DataTypes.STRING,
        allowNull: true,
    },

    tp : {
        type : DataTypes.FLOAT,
        allowNull : true,
    },

    fp : {
        type : DataTypes.FLOAT,
        allowNull : false,
    },

    fn : {
        type : DataTypes.FLOAT,
        allowNull: true,
    },

    tn : {
        type : DataTypes.FLOAT,
        allowNull: true,
    },

    sensivility : {
        type : DataTypes.FLOAT,
        allowNull: true,
    },

    specificity : {
        type : DataTypes.FLOAT,
        allowNull: true,
    },

    vpn : {
        type : DataTypes.FLOAT,
        allowNull: true,
    },

    vpp : {
        type : DataTypes.FLOAT,
        allowNull: true,
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },
},{
    timestamps : false
})



      



