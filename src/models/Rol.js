import { DataTypes} from 'sequelize'
import { sequelize } from '../database/database.js'

/*
 * Proyecto: FonoDX
 * Modelo : Rol (roles)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 


export const Rol =  sequelize.define("roles",{
        id : {
            type : DataTypes.BIGINT,
            primaryKey : true,
            autoIncrement: true
        },

        name : {
            type : DataTypes.STRING,
            allowNull: false
        },

        createdAt: {
            type : DataTypes.DATE,
            defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
            allowNull: true,
        },
},{
    timestamps : false
});
