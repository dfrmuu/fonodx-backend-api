import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : Usuario (users)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 

export const Usuario = sequelize.define('users',{
    id : {
        type : DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
    },

    ci: {
        type: DataTypes.STRING,
        allowNull: true
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    last_name : {
        type : DataTypes.STRING,
        allowNull : false
    },

    status : {
        type : DataTypes.STRING,
        allowNull : false
    },

    email : {
        type : DataTypes.STRING,
        allowNull : false
    },

    phone: {
        type : DataTypes.STRING,
        allowNull: false
    },

    city : {
        type : DataTypes.STRING,
        allowNull : true
    },

    address: {
        type : DataTypes.STRING,
        allowNull: true
    },

    password : {
        type: DataTypes.STRING,
        allowNull: false,
    },

    default_pass : {
        type: DataTypes.STRING,
        allowNull : true
    },

    RoleID : {
        type : DataTypes.BIGINT,
        allowNull: false,
    },

    name_user : {
        type : DataTypes.STRING,
        allowNull: false,
    },

    avatar : {
        type : DataTypes.STRING,
        allowNull: true,
    },


    ip : {
        type : DataTypes.STRING,
        allowNull: true,
    },

    connected : {
        type : DataTypes.INTEGER,
        defaultValue : 0,
        allowNull: false,
    },

    accepted_policy : {
        type : DataTypes.BOOLEAN,
        allowNull : true
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },


}, {
    timestamps: false,
});
