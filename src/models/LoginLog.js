import { DataTypes, Sequelize } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : LoginLog (login_logs)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 

export const LoginLog = sequelize.define("login_logs", {
    id : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        primaryKey : true,
    },

    UserID : {
        type : DataTypes.UUID,
        allowNull : false
    },


    device : {
        type : DataTypes.STRING,
        allowNull: true
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },

},{
    timestamps: false
})