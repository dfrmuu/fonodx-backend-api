import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";
import { Paciente } from "./Paciente.js";

/*
 * Proyecto: FonoDX
 * Modelo : Senal (signals)
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 

export const Senal = sequelize.define("signals", {
    id : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        primaryKey : true,
    },

    PatientID : {
        type : DataTypes.UUID,
        allowNull : false
    },

    focus : {
        type : DataTypes.STRING,
        allowNull : false
    },

    reference : {
        type : DataTypes.STRING,
        allowNull : false
    },

    status : {
        type : DataTypes.STRING,
        allowNull : false
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },

}, {
    timestamps : false
})

