import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : Actividad (activities)
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 

export const Actividad = sequelize.define('activities', {
    id  : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        allowNull : false,
        primaryKey : true
    },

    UserID : {
        type : DataTypes.UUID,
        allowNull : false,   
    },

    activity : {
        type : DataTypes.STRING,
        allowNull : false
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },

},{
    timestamps : false
})