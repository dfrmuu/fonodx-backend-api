import { DataTypes, Sequelize } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : Invitacion (invitations)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 

export const Invitacion = sequelize.define("invitations", {
    id : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        primaryKey : true,
    },

    email : {
        type : DataTypes.STRING,
        allowNull : true
    },

    user_type : {
        type : DataTypes.STRING,
        allowNull : true
    },


    expirationDate : {
        type : DataTypes.DATE,
        allowNull : false
    },

    AdminID : {
        type : DataTypes.UUID,
        allowNull : false
    },

    status : {
        type : DataTypes.STRING,
        allowNull : true
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },
},{
    timestamps: false
})