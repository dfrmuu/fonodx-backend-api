import { DataTypes, Sequelize } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : ErrorLog (error_logs)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 

export const ErrorLog = sequelize.define("error_logs", {
    id : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        primaryKey : true,
    },

    resource : {
        type : DataTypes.STRING,
        allowNull : true
    },


    action : {
        type : DataTypes.STRING,
        allowNull : true
    },

    info : {
        type : DataTypes.TEXT,
        allowNull : true
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },

},{
    timestamps: false
})