import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : Paciente (patients)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 


export const Paciente = sequelize.define("patients", {
    id : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        primaryKey : true,
    },

    UserID : {
        type : DataTypes.UUID,
        allowNull : false
    },

    age : {
        type : DataTypes.INTEGER,
        allowNull: true,
    },

    initial_classification : {
        type : DataTypes.STRING,
        allowNull : true,
        isIn: [['patologico', 'no patologico']],
    },

    aortic_focus : {
        type : DataTypes.STRING,
        allowNull : false,
        isIn: [['FA','FP','FT','FM']],
    },

    familiar : {
        type : DataTypes.STRING,
        allowNull: true,
    },

    familiar_phone : {
        type : DataTypes.STRING,
        allowNull: true,
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },


},{
    timestamps : false
})



      



