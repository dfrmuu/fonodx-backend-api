import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";
import { Paciente } from "./Paciente.js";

/*
 * Proyecto: FonoDX
 * Modelo : TokenContrasena (recover_tokens)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 

export const TokenContrasena = sequelize.define("recover_tokens", {
    id : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        primaryKey : true,
    },

    UserID : {
        type : DataTypes.UUID,
        allowNull : false
    },

    token : {
        type : DataTypes.STRING,
        allowNull : false
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },

    expirationDate: {
        type : DataTypes.DATE,
        allowNull: false,
    },

}, {
    timestamps : false
})

