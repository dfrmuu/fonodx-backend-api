import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";

/*
 * Proyecto: FonoDX
 * Modelo : Permiso (permissions)
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 */ 

export const Permiso = sequelize.define('permissions', {
    id  : {
        type : DataTypes.UUID,
        defaultValue : DataTypes.UUIDV4,
        allowNull : false,
        primaryKey : true
    },

    RoleID : {
        type : DataTypes.BIGINT,
        allowNull : false,   
    },

    resource : {
        type : DataTypes.STRING,
        allowNull : false
    },

    action : {
        type : DataTypes.STRING,
        allowNull : false
    },

    createdAt: {
        type : DataTypes.DATE,
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: true,
    },

},{
    timestamps : false
})