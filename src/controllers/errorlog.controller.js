import { Op } from "sequelize"
import { sequelize } from "../database/database.js"
import { ErrorLog } from "../models/ErrorLog.js"
import { Usuario } from "../models/Usuario.js"

/*
 * Proyecto: FonoDX
 * Controlador: Log de errores
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Operaciones tabla "error_logs"
 */

const RECURSO = "logErrores"


//Recupera los logs de errores de la base de datos
export const traerLogsErrorGeneral = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerLogsErrorGeneral"

    try {
    
        //Trae los top de errores
        const topErrores = await ErrorLog.findAll({
            attributes: [
              'resource', 
              'action',
              'info',
              'createdAt',
              [sequelize.fn('COUNT', sequelize.col('id')), 'cantidad']
            ],
            group: ['resource'],
            order: [[sequelize.col('cantidad'), 'DESC']],
            limit: 4
        });

        //Trae los logs de errores con cantidad de veces que se repiten
        const listaLogsDeErrorTotal = await ErrorLog.findAll({
            attributes: [
              'resource',
              'action',
              'info',
              'createdAt',
              [sequelize.fn('COUNT', sequelize.col('id')), 'cantidad']    
            ],
            group: ['resource', 'action', 'info'],
            order: [
                [sequelize.col('createdAt'),'DESC']
            ] 
        });


        return res.status(200).json({listaLogs : listaLogsDeErrorTotal, topErrores : topErrores})

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error en el servidor"})
    }
}

//Recupera una lista de recursos y errores en los logs
export const traerRecursos = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerRecursos"

    try {
        
        const listaRecursos = await ErrorLog.findAll({
            attributes: [
              'resource' 
            ],
            group: ['resource'] 
        });

        //¿Esta vacia?
        if(listaRecursos.length > 0 ){
            return res.status(200).send(listaRecursos)
        } 

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error en el servidor"})
    }
}


//Registra un log de error
export const registrarLogError = async(data) => {
    try {
        
        //Crea un transacción con la operación
        await sequelize.transaction( async (t) => {

            //Crea el log
            const crearLog = await ErrorLog.create(data, {transaction : t})
            
            if(crearLog != null){
                //Retorna verdadero si se crea el log
                return true
            }
        })

    } catch (error) {

        //Retorna falso si no se puede crear
        return false
    }
}

//Filtrar los logs de error
export const filtrarErrorLogs = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "filtrarErrorLogs"

    try {
     
        //Recupera los datos en la petición
        const criterio = req.criterio
        const dato = req.dato

        //Filtra los logs
        const consultaErrorLogs = await ErrorLog.findAll({
            attributes: [
                'resource',
                'action',
                'info',
                'createdAt',
                [sequelize.fn('COUNT', sequelize.col('id')), 'cantidad']    
            ],
            group: ['resource', 'action', 'info'],
            order: [
                  [sequelize.col('createdAt'),'DESC']
            ],
            where : {
                [criterio] : dato
            }
        })

        if(consultaErrorLogs.length == 0){
            return res.status(404).json({Error : "No se encontraron datos."})
        } else {
            return res.status(200).send(consultaErrorLogs)
        }

    } catch (error) {
        
        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error :"Ocurrio un error al recuperar los registros de inicio de sesión"})
    } 
 }