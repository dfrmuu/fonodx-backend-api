import {config} from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { Invitacion } from "../models/Invitacion.js"
import { Usuario } from "../models/Usuario.js"
import { Administrativo } from "../models/Administrativo.js"
import { enviarEmail } from "./mailer.controller.js";
import handlebars  from 'handlebars'
import { sequelize } from "../database/database.js";
import { DateTime } from "luxon";
import * as fs from 'fs'
import { registrarLogError } from './errorlog.controller.js';

/*
 * Proyecto: FonoDX
 * Controlador: Invitaciones
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Operaciones en tabla "invitations".
 */


//Configura las variables de entorno
config()

const RECURSO = "invitaciones"

//Recupera las invitaciones en la base de datos
export const misInvitaciones = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "misInvitaciones"

    try{

        //Recupera datos en la petición
        const idAdmin = req.user.IdAdmin

        //Trae las invitaciones
        const misInvitaciones = await Invitacion.findAll({
            where : {
                AdminID : idAdmin
            }
        })

        return res.status(200).send(misInvitaciones)

    }catch(error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)


        return res.status(500).json({Error : "Ocurrio un error en el servidor."})
    }
}

//Recupera datos de una invitación
export const traerInvitacion = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerInvitacion"

    try {

        //Recupera datos en la petición
        const invitacion = req.invitacion

        return res.status(200).send(invitacion)

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor."})
    }
}

//Crea una invitación 
export const crearInvitacion = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "crearInvitacion"

    try{

        //Crea una transacción
        await sequelize.transaction( async (t) => {

            //Recupera datos de la petición
            const data = req.form_data
            //Trae la url de FrontEND
            const urlFRONT = process.env.URL_FRONT
            //Trae la url de imagenes
            const urlIMG = process.env.URL_IMGS

            //Crea una invitación nueva
            const nuevaInvitacion = await Invitacion.create(data, {transaction : t})
            var rol
    
            if(nuevaInvitacion != null){

                //Lee plantilla html de invitación
                const html = fs.readFileSync('src/mailer/invitacion.html', 'utf-8');
    
                //Según el tipo de usuario de la invitación
                switch (nuevaInvitacion.user_type) {

                    case "1":
                        rol = "PACIENTE" 
                    break;

                    case "2":
                        rol = "MEDICO"
                    break;
                
                    case "3":
                        rol = "INVESTIGADOR"
                    break;
    
                    case "4":
                        rol = "ADMINISTRADOR"
                    break;
                }
    
                //Compila la plantilla
                const template = handlebars.compile(html)
                
                //Carga los datos del correo
                const datosCorreoRegistro = {
                    "correo": data.email,
                    "rol" : rol,
                    "fecha_caducidad" : data.expirationDate,
                    "link" : urlFRONT + "/register"
                }

                //Compila la plantilla con los datos
                const htmlAEnviar = template(datosCorreoRegistro)
        
                //Envia el correo
                const email = await enviarEmail(data.email,"Invitación en FonoDX", htmlAEnviar)
    
                if(email == "enviado"){
                    return res.status(200).json({Message : "Se envio la invitación exitosamente"}) 
                } else {
                    //Cancela la transacción
                    t.rollback();
                    return res.status(500).json({Error : "Al parecer ocurrio un error enviando la invitación, intentelo más tarde."})
                }
            }
    
        })

    }catch(error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor."})
    }
}

//Editar una invitación existente
export const editarInvitacion = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "editarInvitacion"

    try{

        //Crea una transacción
        await sequelize.transaction( async (t) => {

            //Recupera datos de la petición
            const data = req.data
            const invitacion = req.invitacion
            const rolActualizado = req.rolActualizado
            //Trae la url del FrontEND
            const urlFRONT = process.env.URL_FRONT

            //Actualiza la invitación
            const invitacionActualizada = await Invitacion.update(data,{
                where : {
                    id : invitacion.id
                }
            },{
                transaction : t
            })


            var rol
    
            if(invitacionActualizada != null){

                //¿El email es diferente al que existia anteriormente en la invitación?
                if(data.email != null && data.email != undefined){ 
                    const html = fs.readFileSync('src/mailer/invitacion.html', 'utf-8');
    
                    //Según el tipo de usuario en la invitación
                    switch (data.user_type) {
                        case "1":
                            rol = "PACIENTE" 
                        break;

                        case "2":
                            rol = "MEDICO"
                        break;
                
                        case "3":
                            rol = "INVESTIGADOR"
                        break;
        
                        case "4":
                            rol = "ADMINISTRADOR"
                        break;
                    }
        
                    //Lee plantilla html de invitación
                    const template = handlebars.compile(html)
                    
                    //Carga los datos del correo
                    const datosCorreoRegistro = {
                        "correo": data.email,
                        "rol" : rol,
                        "fecha_caducidad" : invitacion.expirationDate,
                        "link" : urlFRONT + "/register"
                    }
                
                    //Compila la plantilla
                    const htmlAEnviar = template(datosCorreoRegistro)
            
                    //Envia el correo
                    const email = await enviarEmail(data.email,"Invitación en FonoDX", htmlAEnviar)
        
                    if(email == "enviado"){
                        return res.status(200).json({Message : "Se editó la invitación exitosamente"}) 
                    } else {
                        t.rollback();
                        return res.status(500).json({Error : "Al parecer ocurrio un error enviando la invitación, intentelo más tarde."})
                    }

                } else {

                    //¿El rol es diferente al que ya existia en la invitación?
                    if(rolActualizado == true){   

                        //Lee la plantilla del correo de invitación
                        const html = fs.readFileSync('src/mailer/invitacion.html', 'utf-8');
                        var correoParaEnvio 

                        //Revisa si es un correo diferente, o el mismo de antes.
                        if(data.email != null && data.email != undefined){
                            correoParaEnvio = data.email
                        } else {
                            correoParaEnvio = invitacion.email
                        }

                        //Según el tipo de usuario
                        switch (data.user_type) {
                            case "1":
                                rol = "PACIENTE" 
                            break;
            
                            case "2":
                                rol = "MEDICO"
                            break;
                        
                            case "3":
                                rol = "INVESTIGADOR"
                            break;

                            case "4":
                                rol = "ADMINISTRADOR"
                            break;
                        }
            
                        //Compila plantilla
                        const template = handlebars.compile(html)
                        
                        //Agrega datos a la plantilla de correo
                        const datosCorreoRegistro = {
                            "correo": correoParaEnvio,  
                            "rol" : rol,
                            "fecha_caducidad" : invitacion.expirationDate,
                            "link" : urlFRONT + "/register"
                        }

                        //Carga la plantilla con los datos del correo
                        const htmlAEnviar = template(datosCorreoRegistro)
                
                        //Envia correo
                        const email = await enviarEmail(correoParaEnvio,"Invitación en FonoDX", htmlAEnviar)
            
                        if(email == "enviado"){
                            return res.status(200).json({Message : "Se editó la invitación exitosamente"}) 
                        } else {
                            //La idea es que este rollback cancele la transacción.
                            t.rollback();  
                            return res.status(500).json({Error : "Al parecer ocurrio un error enviando la invitación, intentelo más tarde."})
                        }
                    } else {
                        return res.status(200).json({Message : "Se editó la invitación"})
                    }
                }

            }
    
        })

    }catch(error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor."})
    }
}

//Elimina una invitación
export const eliminarInvitacion = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "eliminarInvitacion"

    try {
        
        //Recupera datos de la petición
        const invitacion = req.invitacion

        //Borra la invitación
        const borrarInvitacion = await Invitacion.destroy({
            where : {
                id : invitacion
            }
        })

        if(borrarInvitacion){
            return res.status(200).json({Message : "Se borró la invitación."})
        }


    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)
        
        return res.status(500).json({Error : "Ocurrio un error en el servidor."})
    }
}

export const filtrarInvitaciones = async(req,res) => {
    //Esto es para agregar el origen del error en el log
    const ACCION = "filtrarInvitaciones"

    try {
        
        //Recupera datos en la petición
        const criterio = req.criterio
        const idAdmin = req.user.IdAdmin
        const data = req.data
        let whereCondition

        if(criterio != "correo") {
            whereCondition = {
                [criterio] : data,
                AdminID : idAdmin
            }
        } else if ( criterio == "correo"){
            whereCondition = {
                email : {
                    [Op.like] : `%${dato}%`
                },
                AdminID : idAdmin
            }
        }

        const consultaInvitaciones = await Invitacion.findAll({
            where : whereCondition
        })

        if(consultaInvitaciones.length > 0){
            return res.status(200).send(consultaInvitaciones)
        } else {
            return res.status(404).json({Error : "No hay datos según los criterios"})
        }


    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)
        
        return res.status(500).json({Error : "Ocurrio un error en el servidor."})
    }
}