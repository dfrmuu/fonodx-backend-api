import { Sequelize,Op } from "sequelize";
import { Medico } from "../models/Medico.js";
import { Paciente } from "../models/Paciente.js";
import { Senal } from "../models/Senal.js";
import { Resultado } from "../models/Resultado.js";
import { registrarLogError } from "./errorlog.controller.js";
import { DateTime } from "luxon";

/*
 * Proyecto: FonoDX
 * Controlador: Auscultaciones / señales
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Operaciones relacionadas a la tabla de señales / auscultaciones (creación, edición, entre otras)..
 */

const RECURSO = "auscultaciones"

//Recupera las auscultaciones de un paciente en la base de datos
export const traerSenales = async (req, res) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "traerSenales"

    try {

        //Recupera la id del paciente
        const id = req.paciente_id;

        //Consulta las señales del paciente
        const señalesPaciente = await Senal.findAll({
          attributes: [
            [
              Sequelize.literal('DATE_FORMAT(createdAt, "%Y-%m-%d")'),
              'createdAt', // Truncamos la fecha a "Y-m-d"
            ],
          'status',
        ],
        where: {
          PatientID: id,
        },
          group: [Sequelize.literal('DATE_FORMAT(createdAt, "%Y-%m-%d")')], // Agrupamos por "Y-m-d"
          order: [['createdAt', 'DESC']],
        });

        //Envia las señales del paciente
        return res.status(200).send(señalesPaciente);

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = {
          resource : RECURSO,
          action : ACCION,
          info : error.message
        }

        const registroError = await registrarLogError(dataError)
      
        return res.status(500).json({ Error: 'Ocurrió un error al traer las señales.' });
    }
};


//Recupera los datos de una auscultación
export const traerSenal = async(req,res) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "traerSenal"

    try {

        //Recupera la auscultación
        const auscultacion = req.auscultacion

        //Envia la auscultación
        return res.status(200).send(auscultacion)

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
          resource : RECURSO,
          action : ACCION,
          info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({ Error: 'Ocurrió un error al traer la señal.' });
    }
}

//Edita una auscultación
export const editarSenal = async(req,res) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "editarSenal"

    try {
      
        //Recupera los auscultación y los datos que vienen desde el formulario
        const auscultacion = req.auscultacion
        const data = req.data

        //Edita la auscultación
        const actualizarSenal = await Senal.update(data, {
          where : {
            id : auscultacion
          }
        })

        //¿Se editó?
        if(actualizarSenal != null){
          return res.status(200).json({Message : "Se editó la señal."})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
          resource : RECURSO,
          action : ACCION,
          info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({ Error: 'Ocurrió un error al traer la señal.' });
    }
}

//Elimina una auscultación
export const eliminarSenal = async(req,res) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "eliminarSenal"

    try {

        //Recupera los datos de la auscultación
        const id = req.auscultacion

        //Borra la auscultación
        const borrarSenal = await Senal.destroy({
          where : {
            id : id
          }
        })

        //¿Se eliminó?
        if(borrarSenal != null){
          return res.status(200).json({Message : "Se borró la señal."})
        }


    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
          resource : RECURSO,
          action : ACCION,
          info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({ Error: 'Ocurrió un error al traer la señal.' });
    }
}

//Filtra los datos de auscultaciones por fecha y id del paciente
export const traerSenalPorFechaID = async(req,res) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "traerSenalPorFechaID"

    try{

        //Recupera la id del paciente
        const id = req.paciente_id

        //Recupera la fecha de creación
        const {fecha_creacion} = req.body

        //Recupera todas las auscultaciones del paciente a partir de la fechas y la id del paciente
        const señalesPaciente = await Senal.findAll({
          where: Sequelize.literal(`signals.PatientID = '${id}' AND DATE(signals.createdAt) = DATE(STR_TO_DATE('${fecha_creacion}', '%Y-%m-%d')) AND signals.PatientID = '${id}'`),
          order: [
              ['createdAt', 'DESC'],
          ],
          include: [{
              model: Medico,
              as: 'favorite_signals',
          }, {
              model : Resultado,
              as : 'results'
          }],
        });

        //Envia las auscultaciones filtradas
        return res.status(200).send(señalesPaciente)

    }catch(error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
          resource : RECURSO,
          action : ACCION,
          info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Al parecer ocurrio un error al traer los registros"})
    }
}




//Crea una nueva auscultación
export const registrarAuscultacion = async (req,res) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "registrarAuscultacion"

    try {
      
        //Recupera los datos de la auscultación
        const señal = req.sign_data

        //Recupera la ruta del archivo de audio
        const auscultacion = req.audio

        //Asigna la referencia a la auscultación
        señal.reference = auscultacion

        //Crea la nueva auscultación
        const señalNueva = await Senal.create(señal)

        return res.status(200).json({Message : "Se registro la auscultación"})

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
          resource : RECURSO,
          action : ACCION,
          info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(403).json({Error : "Ocurrio un error al registrar la señal."})
    }
}

//Filtra los registros de las auscultaciones en la base de datos
export const filtrarAuscultaciones = async(req,res) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "filtrarAuscultaciones"

    try {
      
        //Recupera los datos en el formulario
        const paciente = req.paciente
        const fecha = req.fecha

        //Filtra las auscultaciones
        const auscultacionesFiltradas = await Senal.findAll({
          attributes: [
              [Sequelize.fn('DATE_FORMAT', Sequelize.col('createdAt'), '%Y-%m-%d'), 'createdAt']
          ],
          group: [Sequelize.literal('DATE_FORMAT(createdAt, "%Y-%m-%d")')],
          where: {
              createdAt: {
                [Sequelize.Op.between]: [
                    `${fecha} 00:00:00`,
                    `${fecha} 23:59:59`
                ]
              },
              PatientID: paciente
          },
          order: [['createdAt', 'DESC']],
        });

        //¿Hay datos?
        if(auscultacionesFiltradas.length > 0){
          return res.status(200).send(auscultacionesFiltradas)
        } else {
          return res.status(404).json({Error : "No hay datos según el criterio"})
        }

    } catch (error) {
        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error"})   
    }
}


export const recuperarFechasAuscultaciones = async(req,res) => {
    try {

        const paciente = req.paciente

        //Consulta las señales del paciente
        const fechasAuscultaciones = await Senal.findAll({
          attributes: [
            [
              Sequelize.literal('DATE_FORMAT(createdAt, "%Y-%m-%d")'),
              'createdAt', // Truncamos la fecha a "Y-m-d"
            ],
        ],
        where: {
          PatientID: paciente,
        },
          group: [Sequelize.literal('DATE_FORMAT(createdAt, "%Y-%m-%d")')], // Agrupamos por "Y-m-d"
          order: [['createdAt', 'DESC']],
        });

        if(fechasAuscultaciones.length > 0 ){
          return res.status(200).send(fechasAuscultaciones)
        }

    } catch (error) {

          //Se arma el objeto para el registro en el log
          const dataError = { 
              resource : RECURSO,
              action : ACCION,
              info : error.message
          }

          const registroError = await registrarLogError(dataError)

          return res.status(500).json({Error : "Ocurrio un error"})     
    }
}