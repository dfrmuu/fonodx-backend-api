import { Actividad } from '../models/Actividad.js'
import { Rol } from '../models/Rol.js'
import { Senal } from '../models/Senal.js'
import { Sequelize,Op } from "sequelize";
import { Usuario } from '../models/Usuario.js';
import { registrarLogError } from './errorlog.controller.js';

/*
 * Proyecto: FonoDX
 * Controlador: Actividades
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Operaciones tabla 'activities'.
 */

const RECURSO = "actividades"

//Recupera los datos de un usuario
export const actividadUsuario = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "actividadUsuario"

    try {

        //Trae los datos de la petición
        const ID_Usuario = req.usuario
        const ID_Rol = req.user.RoleID

        //Según el rol trae las actividades correspondientes
        if(ID_Rol == 1){//Paciente

            const conteoAuscultaciones = await Senal.findAll({
                attributes : [
                    [Sequelize.fn('COUNT', 'id'), 'SignalCount'], // Contar usuarios en cada grupo   
                ],
                where: {
                    PatientID : req.user.IdPaciente
                },
            })

            return res.status(200).send(conteoAuscultaciones)

        }if (ID_Rol == 2) { //Médico

            //Recupera las actividades
            const actividades = await Actividad.findAll({
                attributes: [
                    [Sequelize.fn('count', Sequelize.literal(`CASE WHEN activity = 'Auscultacion' THEN 1 ELSE NULL END`)), 'count_auscultaciones'],
                    [Sequelize.fn('count', Sequelize.literal(`CASE WHEN activity = 'Paciente' THEN 1 ELSE NULL END`)), 'count_pacientes'],
                ],
                group: ['UserID'],
                where : {
                    UserID : ID_Usuario
                }
             })

             return res.status(200).send(actividades)

        } if (ID_Rol == 4) { //Administrador

            const conteoUsuarios = await Usuario.findAll({
                attributes: [
                  'RoleID', // Columna por la que quieres agrupar
                  [Sequelize.fn('COUNT', 'RoleID'), 'UserCount'], // Contar usuarios en cada grupo
                ],
                include: [
                  {
                    model: Rol, // Modelo de Roles
                  },
                ],
                where: {
                  createdAt: {
                    [Op.gte]: Sequelize.literal(`DATE_FORMAT(NOW(), '%Y-01-01')`), // Año actual-01-01 (1 de enero)
                    [Op.lt]: Sequelize.literal(`DATE_FORMAT(NOW(), '%Y-12-31')`), // Año actual-12-31 (31 de diciembre)
                  },
                },
                group: ['RoleID'],
            })


            const conteoAuscultaciones = await Senal.findAll({
                attributes : [
                    [Sequelize.fn('COUNT', 'id'), 'SignalCount'], // Contar usuarios en cada grupo   
                    'focus',
                ],
                where: {
                    createdAt: {
                      [Op.gte]: Sequelize.literal(`DATE_FORMAT(NOW(), '%Y-01-01')`), // Año actual-01-01 (1 de enero)
                      [Op.lt]: Sequelize.literal(`DATE_FORMAT(NOW(), '%Y-12-31')`), // Año actual-12-31 (31 de diciembre)
                    },
                },
                group: ['focus'],
            })


            return res.status(200).json({ conteoUsuarios: conteoUsuarios, conteoAuscultaciones : conteoAuscultaciones})

        }


    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor"})
    }
}

//Guarda la actividad de un usuario
export const guardarActividad = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "guardarActividad"

    try {

        //Recupera los datos en la petición
        const data = req.form_data

        //Crea la actividad
        const nuevaActividad = await Actividad.create(data)
        return res.status(200)

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor"})
    }
}
