import {config} from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
config()
import * as bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken';
import { registrarLogError } from './errorlog.controller.js';


//Asigna un token a partir de la clave del desarrollo
export const AsignarToken = async (payload) => {

    try{
        const token = jwt.sign(payload, process.env.JWT_SECRET_KEY, { expiresIn: process.env.JWT_KEY_EXPIRES_IN});
        return token;
    }catch(error){
        console.log(error)
    }
}
