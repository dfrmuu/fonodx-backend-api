import { Op, Sequelize } from 'sequelize';
import { LoginLog } from '../models/LoginLog.js'
import { Usuario } from '../models/Usuario.js'
import { registrarLogError } from './errorlog.controller.js';

/*
 * Proyecto: FonoDX
 * Capa de negocio: Log de inicios de sesión
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Operaciones tabla "login_logs"
 */

const RECURSO = "logLogin"

//Recupera los logs de inicio de sesión
export const traerLoginLogs = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerLoginLogs"

    try {

        //Trae el usuario en la petición
        const usuario = req.usuario

        //Recupera los logs de inicio de sesión
        const loginLogs = await LoginLog.findAll({
            attributes: [
              [Sequelize.fn('DATE', Sequelize.col('login_logs.createdAt')), 'createdDate'], // Trunca la fecha a la precisión diaria
              'device',
              [Sequelize.fn('COUNT', Sequelize.col('login_logs.createdAt')), 'ingresos'], // Específica la tabla
            ],
            include: [
              {
                model: Usuario,
                attributes: ['name', 'last_name'],
              },
            ],
            where: {
              UserID: usuario,
            },
            group: ['createdDate', 'device'], // Agrupa por fecha truncada y dispositivo
            order: ['createdDate', 'device'], // Opcional, ordenar los resultados
        });
          
        //¿Hay logs?
        if(loginLogs.length > 0){
            return res.status(200).send(loginLogs)
        }else{
            return res.status(400).json({Error : "No hay registros de inicio de sesión disponibles"})
        }

    } catch (error) {
        //Se arma el objeto para el registro en el log
        const dataError = { 
          resource : RECURSO,
          action : ACCION,
          info : error.message
        }

        const registroError = await registrarLogError(dataError)
        return res.status(500).json({Error :"Ocurrio un error al recuperar los registros de inicio de sesión"})
    }
}

//Filtra los logs de inicio de sesión
export const filtrarLoginLogs = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "filtrarLoginLogs"

    try {
      
          //Recupera los datos en la petición
          const usuario = req.usuario
          const fecha_inicio = req.fecha_inicio
          const fecha_fin = req.fecha_fin || fecha_inicio

          const fechaInicioFormateada = fecha_inicio + ' 00:00:00'
          const fechaFinFormateada = fecha_fin + ' 23:59:59'

          //Construye el where de la consulta
          const whereCondition = {
              UserID : usuario,

              createdAt : {
                // Filtra por un rango de fechas
                [Op.between]: [fechaInicioFormateada, fechaFinFormateada], 
              }
          }

          //Recupera los registros de inicio de sesión
          const loginLogs = await LoginLog.findAll({
              attributes: [
                [Sequelize.fn('DATE', Sequelize.col('login_logs.createdAt')), 'createdDate'],
                'device',
                [Sequelize.fn('COUNT', Sequelize.col('login_logs.createdAt')), 'ingresos'],
              ],
              include: [
                {
                  model: Usuario,
                  attributes: ['name', 'last_name'],
                },
              ],
              where: whereCondition,
              group: ['createdDate', 'device'],
              order: ['createdDate', 'device'],
          });
            
          //¿Hay logs?
          if(loginLogs.length > 0){
              return res.status(200).send(loginLogs)
          }else{
              return res.status(400).json({Error : "No hay registros de inicio de sesión disponibles según los filtros"})
          }

    } catch (error) {
          //Se arma el objeto para el registro en el log
          const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
          }

          const registroError = await registrarLogError(dataError)
          
          return res.status(500).json({Error :"Ocurrio un error al recuperar los registros de inicio de sesión"})
    } 
}