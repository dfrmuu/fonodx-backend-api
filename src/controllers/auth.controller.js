import {config} from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { Usuario } from "../models/Usuario.js"
import { AsignarToken } from "./token.controller.js";
import { enviarEmail } from "./mailer.controller.js";
import { Invitacion } from "../models/Invitacion.js";
import { Investigador } from '../models/Investigador.js'
import { LoginLog } from '../models/LoginLog.js'
import { Medico } from "../models/Medico.js";
import { DateTime } from "luxon";
import { sequelize } from "../database/database.js";
import { TokenContrasena } from '../models/TokenContrasena.js'
import { registrarLogError } from "./errorlog.controller.js";
import bcrypt from "bcrypt";
import * as fs from 'fs'
import handlebars  from 'handlebars'
import { Administrativo } from "../models/Administrativo.js";


/*
 * Proyecto: FonoDX
 * Controlador: Autentificación
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 01
 * Descripción: Operaciones de autentificación.
 */


//Carga de variables de entorno
config() 

const RECURSO = "autentificacion"

//Inicio de sesión
export const iniciarSesion = async (req,res) => { 

    try {
        //Contraseña que envia el login
        const {password} = req.user_login                         
        const device = req.device
        const permisos = req.permisos
        const datosRol = req.role_info
        //Datos que vienen del API
        const {RoleID} = req.user_data        
        //Contraseña del usuario que viene del API                      
        const contraseña_login = req.user_data.password    
        //Que se va a encriptar en el token       
        var payload                                                 

        //Revisión de rol
        if(RoleID == 1){ //PACIENTE

            //Recupera los datos del usuario
            const {id,name_user, name , last_name} = req.user_data
            const {idPaciente,familiar,telefonoFamiliar} = req.role_info.info

            //Datos a partir del rol al que pertenezca
            payload = {  
                Id : id,
                Nombre_Usuario : name_user,
                Nombre_Completo : name + " " + last_name,
                RoleID : RoleID,
                IdPaciente : idPaciente,
                Familiar : familiar,
                FamiliarTelefono : telefonoFamiliar,
                permisos : permisos   
            }

        }if(RoleID == 2){   //MÉDICO

            //Recupera los datos del usuario
            const {id,name_user, name , last_name} = req.user_data
            const {idMedico,cargo,area} = req.role_info.info

            //Datos a partir del rol al que pertenezca
            payload = {  
                Id : id,
                Nombre_Usuario : name_user,
                Nombre_Completo : name + " " + last_name,
                RoleID : RoleID,
                IdMedico : idMedico,
                Cargo : cargo,
                Area : area,
                permisos : permisos   
            }

        }if(RoleID == 3){ //INVESTIGADOR

            //Recupera los datos del usuario
            const {id,name_user,name, last_name} = req.user_data
            const {idInvestigador,cargo,area} = req.role_info.info


            //Datos a partir del rol al que pertenezca
            payload = {  
                Id : id,
                Nombre_Usuario : name_user,
                Nombre_Completo : name + " " + last_name,
                RoleID : RoleID,
                IdInvestigador : idInvestigador,
                Cargo : cargo,
                Area : area,
                permisos : permisos      
            }   

        } if (RoleID  == 4){ //INVESTIGADOR

            const {id,name_user,name, last_name} = req.user_data
            const {idAdmin,cargo,area} = req.role_info.info

            //Datos a partir del rol al que pertenezca
            payload = { 
                Id : id,
                Nombre_Usuario : name_user,
                Nombre_Completo : name + " " + last_name,
                RoleID : RoleID,
                IdAdmin : idAdmin,
                Cargo : cargo,
                Area : area,
                permisos : permisos      
            }            
        }

        //Compara la contraseña de inicio de sesión
        const comparacionContraseña = await bcrypt.compare(password, contraseña_login); //Compara la contraseña en la base de datos con la original

        //Revisa la contraseña
        if (comparacionContraseña) {

            //Le asigna un token
            const accessToken = await AsignarToken(payload);

            //Se pudo asignar un token?   <-- AQUI en teoria se inicia sesión, por lo cual voy a agregar el log de inico de sesión.
            if(accessToken){ 

                //Agrega la cookie a los headers
                res.cookie('token', accessToken, { httpOnly: false, secure: true, sameSite: 'Strict', maxAge : 5 * 60 * 60 * 1000})

                //Registra un inicio de sesión al usuario
                const logInicioSesion = await LoginLog.create({
                    UserID : req.user_data.id,
                    device : device
                })

                if(logInicioSesion){
                    return res.status(202).send(accessToken)
                } else {
                    return res.status(500).json({Error : "Ocurrio un error al iniciar sesión"})
                }

            } else {
                return res.status(500).json({Error : "No se pudo asignar un token"}) //No se pudo asignar un token
            }
        } else {
            return res.status(400).json({Error : "Datos incorrectos (contraseña invalida)"}) //La comparación de contraseña devuelve false
        }

    } catch (error) {
        console.log(error)
        return res.status(404).json({Error: "Ocurrio un error al iniciar sesión."})  //El usuario no se encuentra en la base de datos
    }
}


//Registro de usuario nuevo (a partir de invitación)
export const registroUsuario = async(req,res) => {
    try {
        
        //Recupera los datos de la validación (datos del usuario, datos del rol)
        const data = req.data
        const jobdata = req.jobdata
        const invitacion = req.invitacion
        const urlFRONT = process.env.URL_FRONT


        //Segun el rol
        switch (data.RoleID) {
            case 1:
                //Aquí iria el apartado de paciente, si se requiere
            break;

            case 2:
                //Mediante transacción, se crea usuario, medico y se cambia el estado de la invitación
                await sequelize.transaction( async (t) => {
                    const nuevoUsuario = await Usuario.create(data, {transaction : t})
                    jobdata.UserID = nuevoUsuario.id
                    const nuevoMedico = await Medico.create(jobdata, {transaction : t})
                    invitacion.status = "aceptada"
                    await invitacion.save({transaction : t})
                })
            break;

            case 3:
                //Mediante transacción, se crea usuario, investigador y se cambia el estado de la invitación
                await sequelize.transaction( async (t) => {
                    const nuevoUsuario = await Usuario.create(data, {transaction : t})
                    jobdata.UserID = nuevoUsuario.id
                    const nuevoInvestigador = await Investigador.create(jobdata, {transaction : t})
                    invitacion.status = "aceptada"
                    await invitacion.save({transaction : t})

                })
            break;

            case 4:
                //Mediante transacción, se crea usuario, administrador y se cambia el estado de la invitación
                await sequelize.transaction( async (t) => {
                    const nuevoUsuario = await Usuario.create(data, {transaction : t})
                    jobdata.UserID = nuevoUsuario.id
                    const nuevoAdministrador = await Administrativo.create(jobdata, {transaction : t})
                    invitacion.status = "aceptada"
                    await invitacion.save({transaction : t})
                })
            break;

        }


        //Lee la plantilla del correo
        const html = fs.readFileSync('src/mailer/registro.html', 'utf-8');

        //Compila la plantilla
        const template = handlebars.compile(html)

        //Datos del correo
        const datosCorreoRegistro = {
            "nombre_completo": data.name + " " + data.last_name,
            "link" : urlFRONT + "/login"
        }

        //Envia los datos a la plantilla para visualizarlos en el correo
        const htmlAEnviar = template(datosCorreoRegistro)
            

        //Envia el correo
        const email = await enviarEmail(
            data.email,
            "¡Bienvenido a FonoDX!",
            htmlAEnviar
        )
            
        //¿Se envio
        if(email == "enviado"){
            return res.status(200).json({Message : "Se registro correctamente."}) 
        } else {
            return res.status(500).json({Error : "Al parecer ocurrio un error, intentelo más tarde."})
        }
        

    } catch (error) {
        console.log(error)
        return res.status(404).json({Error: "Ocurrio un error al realizar el registro"}) 
    }
}


//Revisa el usuario a partir de su TOKEN
export const revisionUsuario = async(req,res) => {
     if(req.token_status == "valido"){

        if(req.user.RoleID == 1){
            
            const {Nombre_Completo,Nombre_Usuario,RoleID,Id,IdPaciente,Familiar,FamiliarTelefono,permisos} = req.user

            //Retorna objeto con los datos del usuario
            return res.status(202).json({
                Nombre_Completo: Nombre_Completo, 
                Nombre_Usuario : Nombre_Usuario,
                Rol : RoleID, 
                Id: Id, 
                IdPaciente: IdPaciente, 
                Familiar : Familiar,
                FamiliarTelefono : FamiliarTelefono,
                Permisos : permisos
            })

        }if(req.user.RoleID == 2){

            const {Nombre_Completo,Nombre_Usuario,RoleID,Id,IdMedico,Cargo,Area,permisos} = req.user

            //Retorna objeto con los datos del usuario
            return res.status(202).json({
                Nombre_Completo: Nombre_Completo, 
                Nombre_Usuario : Nombre_Usuario,
                Rol : RoleID, 
                Id: Id, 
                IdMedico: IdMedico, 
                Cargo : Cargo,
                Area : Area,
                Permisos : permisos
            })

        } if(req.user.RoleID == 3) {

            const {Nombre_Completo,Nombre_Usuario,RoleID,Id,IdInvestigador,Cargo,Area,permisos} = req.user

            //Retorna objeto con los datos del usuario
            return res.status(202).json({
                Nombre_Completo: Nombre_Completo, 
                Nombre_Usuario : Nombre_Usuario,
                Rol : RoleID, 
                Id: Id, 
                IdInvestigador: IdInvestigador, 
                Cargo : Cargo,
                Area : Area,
                Permisos : permisos
            })    

        } if (req.user.RoleID == 4){

            const {Nombre_Completo,Nombre_Usuario,RoleID,Id,IdAdmin,Cargo,Area,permisos} = req.user

            //Retorna objeto con los datos del usuario
            return res.status(202).json({
                Nombre_Completo: Nombre_Completo, 
                Nombre_Usuario : Nombre_Usuario,
                Rol : RoleID, 
                Id: Id, 
                IdAdmin: IdAdmin, 
                Cargo : Cargo,
                Area : Area,
                Permisos : permisos
            })            

        }
     }
}

//Valida las invitaciones que se han enviado a un correo
export const revisionCorreo = async (req,res) => {
    try{

        //Trae datos de la revisión
        const data = req.data 

        //Retorna los datos de la consulta
        return res.status(202).json({Correo : data.email, TipoUsuario : data.user_type, Invitacion : data.id})

    }catch(error){
        console.log(error)
        return res.status(500).json({Error: "Ocurrio un error al consultar."}) 
    }
}

//Generar token aleatorio
const generarCodigoAleatorio = () => {

    //Caracteres para generar el token
    const caracteres = 'abcdefghijklmnopqrstuvwxyz0123456789$/-';
    let token = '';

    //Mediante un ciclo va agregando caracteres aleatorios en cada posición, hasta completar los 6 caracteres.
    for (let i = 0; i < 6; i++) {
      const indice = Math.floor(Math.random() * caracteres.length); 
      token += caracteres.charAt(indice);
    }

    //Retorna el token
    return token;
}



//Generar token de recuperación de contraseña
export const generarTokenRecuperacion = async (req, res) => {

    const ACCION = 'generarTokenRecuperacion';


    try {

        //Recupera el usuario
        const usuario = req.usuario;
        //Variable que se usa para que los tokens no sean iguales
        let tokenDiferente = false;
        //Variable para el token
        let token;

        //Le da 30 minutos para expirar
        const fecha_expiracion = DateTime.now().setZone('America/Bogota').plus({ minutes: 30 }).toISO();

        //Mientras el token no sea diferente
        do {
            //Genera token
            token = generarCodigoAleatorio(); 

            //Revisa existencia del token
            const revisionToken = await TokenContrasena.findOne({
                where: {
                    token: token
                }
            });

            //¿Es diferente?
            if (!revisionToken) {
                tokenDiferente = true;
            }
        } while (!tokenDiferente);

        //Crea un objeto con los datos del token
        const dataToken = {
            UserID: usuario.id,
            token: token,
            expirationDate: fecha_expiracion
        }


        //Realiza una transacción para hacer el proceso de crear el token y asignarlo a un usuario
        await sequelize.transaction(async (t) => {

            //Crea el token
            const crearToken = await TokenContrasena.create(dataToken, { transaction: t });
            //Le añade el token al usuario
            const añadirToken = await usuario.addToken(crearToken, { transaction: t });

            //¿Se añadio?
            if (añadirToken) {

                //Lee la plantilla del correo
                const html = fs.readFileSync('src/mailer/recuperar.html', 'utf-8');

                //Compila la plantilla
                const template = handlebars.compile(html)

                //Datos del correo
                const datosCorreoRegistro = {
                    "token": dataToken.token
                }
                //Envia los datos a la plantilla para visualizarlos en el correo
                const htmlAEnviar = template(datosCorreoRegistro)
            

                //Envia el correo
                const email = await enviarEmail(
                    usuario.email,
                    "Recuperar contraseña FonoDX",
                    htmlAEnviar
                )
            
                //¿Se envio
                if(email == "enviado"){
                    return res.status(200).json({Message : "Se envió el correo con el token de recuperación a su correo."}) 
                } else {
                    return res.status(500).json({Error : "Al parecer ocurrio un error, intentelo más tarde."})
                }
            }
        });

    } catch (error) {
            
        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({ Error: "Ocurrió un error al intentar recuperar la contraseña." });
    }
}

//Consulta si el token de recuperación es válido
export const revisarTokenRecuperacion = async(req,res) => {

    const ACCION = 'revisarTokenRecuperacion';

    try {

        //Recupera el resultado de si el token existe
        const existeToken = req.existe

        //¿Existe?
        if(existeToken){
            return res.status(200).json({existe : true})
        } else {
            return res.status(500).json({Error : "Este token no es valido."})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error al intenta recuperar la contraseña."}) 
    }
}

//Cambia la clave actual por la nueva del usuario
export const recuperarCredenciales = async(req,res) => {

    const ACCION = 'recuperarCredenciales';

    try {

        //Recupera el usuario
        const usuario = req.usuario
        //Recupera la nueva clave
        const clave = req.clave

        //Hashea la clave
        const claveHash = await bcrypt.hash(clave, 10)

        //Le asigna al usuario la nueva clave
        usuario.password = claveHash

        //Guarda los cambios
        const cambioClave = await usuario.save()

        //¿Se cambio?
        if(cambioClave){
            return res.status(200).json({Message : "Se cambió la clave del usuario"})
        }

    } catch (error ){
        
        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error al intenta recuperar la contraseña."}) 

    }
}
