import { Tarea } from '../models/Tarea.js'
import { Usuario } from '../models/Usuario.js'
import { registrarLogError } from './errorlog.controller.js'

/*
 * Proyecto: FonoDX
 * Controlador : Tareas
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Validación / agrupamiento de datos.
 */



const RECURSO = "tareas"

//Recupera las tareas de un usuario
export const traerTareas = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerTareas"

    try {

        //Recupera la id en la petición
        const id = req.user.Id

        //Trae las tareas del usuario
        const tareasUsuario = await Tarea.findAll({
            where : {
                UserID : id
            }
        })

        //¿Hay tareas?
        if(tareasUsuario.length > 0){
            return res.status(200).send(tareasUsuario)
        }else {
            return res.status(404).json({Error : "No hay registros de tareas en el sistema."})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor"})  
    }
}

//Recupera las tarea de un usuario
export const traerTarea = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerTarea"

    try {

        //Trae los datos en la petición
        const tarea = req.tarea

        //Envia los datos en la petición
        return res.status(200).send(tarea)

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor"})  
    }
}

//Crear una nueva tarea
export const crearTarea = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "crearTarea"

    try {

        //Trae los datos en la petición
        const data = req.data

        //Crea la tarea
        const tareaCreada = await Tarea.create(data)

        //¿Se creó la tarea?
        if(tareaCreada != null){
            return res.status(200).json({Message : "Se creo la tarea"})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor"})  
    }
}

//Edita una tarea
export const editarTarea = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "editarTarea"

    try {

        //Trae los datos en la petición
        const data = req.data
        const tarea = req.tarea

        //Actualiza la tarea
        const tareaCreada = await Tarea.update(data, {
            where : {
                id : tarea
            }
        })

        //Se editó la tarea
        if(tareaCreada != null){
            return res.status(200).json({Message : "Se editó la tarea"})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor"})  
    }
}

//Cambia el estado de la tarea a completada
export const completarTarea = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "completarTarea"

    try {
        
        //Recupera los datos en la petición
        const tarea = req.tarea

        //Cambia el estado y guarda los datos
        tarea.status = true
        tarea.save()

        return res.status(200).json({Message : "Se completo la tarea."})


    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor"})  
    }
}

//Cambia el estado de la tarea a incompleta
export const tareaIncompleta = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "tareaIncompleta"

    try {

        //Recupera los datos en la petición
        const tarea = req.tarea

        //Cambia el estado y guarda los datos
        tarea.status = false
        tarea.save()

        return res.status(200).json({Message : "Se cambio el estado."})


    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor"})  
    }
}

//Elimina una tarea
export const eliminarTarea = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "eliminarTarea"

    try {
        
        //Recupera los datos en la petición
        const tarea = req.tarea

        //Elimina una tarea
        const borrarTarea = await Tarea.destroy({
            where : {
                id : tarea
            }
        })

        return res.status(200).json({Message : "Se eliminó la tarea"})

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)
        
        return res.status(500).json({Error : "Ocurrio un error en el servidor"})  
    }
}