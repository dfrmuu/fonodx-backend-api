import {config} from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { Op } from "sequelize";
import { sequelize } from "../database/database.js";
import { Medico } from "../models/Medico.js";
import { Paciente } from "../models/Paciente.js";
import { Usuario } from "../models/Usuario.js";
import { Resultado } from '../models/Resultado.js'
import { enviarEmail } from "./mailer.controller.js";
import * as fs from 'fs'
import handlebars  from 'handlebars'
import { registrarLogError } from "./errorlog.controller.js";
import { Senal } from '../models/Senal.js';
import { DateTime } from 'luxon';

/*
 * Proyecto: FonoDX
 * Controlador: Pacientes
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Operaciones relacionadas a la tabla de pacientes (creación, edición, entre otras).
 */

//Carga de variables de entorno
config() 

const RECURSO = "pacientes"

//Obtiene la lista de pacientes desde la base de datos
export const traerPacientes = async(req,res) => {

    //Esto es para el seguimiento en caso de error 
    const ACCION = "traerPacientes"             

    try{
        
        //Realiza la consulta en base de datos y recupera todos los valores 
        const pacientes = await Paciente.findAll()
        
        //Retorna un json con los datos de los pacientes
        return res.status(200).send(pacientes)                 

    }catch (error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        //Retorna una respuesta de error
        return res.status(500).json({Error: "Ocurrio un error al recuperar los pacientes."}) 
    }
}

//Filtra los pacientes a partir un parametro (criterio)
export const filtrarPacientes = async (req, res) => {

    //Esto es para el seguimiento en caso de error 
    const ACCION = "filtrarPacientes"

    try {

        //Recupera los valores enviados desde el front (dato - criterio)
        const dato = req.dato;          
        const criterio = req.criterio;
        const idMedico = req.medico;    //Trae la id del médico en la petición.

         //Crea una variable para generar un where dinamico
        let whereCondition;            

        //Revisa el criterio que llega.
        //Si el criterio es nombre, concatena los nombres y apellidos, además de hacer un like para revisar si hay registros similares.
        if (criterio === "name") {             
        
            whereCondition = sequelize.where(                                                          
                sequelize.fn("concat", sequelize.col("name"), ' ', sequelize.col("last_name")),         
                {
                    [Op.like]: `%${dato}%`
                }
            );
        } else {

            //Si el criterio es otro solo hace un like para revisar si existen registros similares
            whereCondition = {                                                                         
                [criterio] : {                                                                         
                    [Op.like]: `%${dato}%`
                }
            };
        } 

        //Recupera los pacientes de la base de datos a partir de los filtros
        const pacientesFiltrados = await Medico.findAll({
            where: {
                id: idMedico
            },
            include: [{
                model: Paciente,
                as: 'patients',
                include: [{
                    model: Usuario,
                    attributes: ['id', 'name', 'last_name', 'ci'],
                    where: whereCondition
                }],
                attributes: ['id', 'initial_classification', 'aortic_focus', 'age'],
            }],
            attributes: ['job_title', 'area']
        });
        
        //Retorna una respuesta exitosa y envia los datos filtrados
        return res.status(200).json(pacientesFiltrados);

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({ Error: "Ocurrió un error al filtrar los pacientes." });
    }
};

//Recupera los pacientes pertenecientes a un médico
export const misPacientes = async (req,res) => {

    //Esto es para el seguimiento en caso de error 
    const ACCION = "misPacientes"

    try{

        //Trae la id del médico en la petición
        const idMedico = req.medico

        //Recupera los datos en la base de datos
        const misPacientes = await Medico.findAll({
            where : {
                id : idMedico
            },
            include :  [{
                model : Paciente,
                as : 'patients',
                include : [{
                    model : Usuario,
                    attributes: ['id', 'name', 'last_name'], 
                }],
                attributes: ['id', 'initial_classification', 'aortic_focus','age'], 
            }],
            attributes : ['job_title','area']
        })

        //Retorna los pacientes
        return res.status(200).send(misPacientes)

    }catch(error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error al recuperar sus pacientes"})
    }
}

//Crea un nuevo paciente
export const crearPaciente = async(req,res) => { 

    //Esto es para el seguimiento en caso de error 
    const ACCION = "crearPaciente"

    try{

        //Creamos una transacción para agrupar las operaciones de forma atomica, la idea es que todas las operaciones en la transacción se cumplan.
        await sequelize.transaction(async(t) => {

            //Trae los datos en la petición
            const data = req.form_data
             //Busca el paciente 
            const pacienteNuevo = await Paciente.create(data, {transaction : t}) 
            //Busca el medico responsable del paciente
            const medico = await Medico.findByPk(req.medico_encargado, {transaction : t})
    
            //Asigna el paciente al médico
            const asignacion = await pacienteNuevo.addDoctor(medico, {transaction : t})
    
            //Revisa si la asignación es éxitosa
            if(asignacion == null){
                return res.status(403).json({Error : "Fallo al asignar."})
            }else{
                return res.status(200).json({Message : "Se registro el paciente exitosamente!"})
            }
        })

    }catch (error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError) 

        return res.status(500).json({Error : "Ocurrio un error al registrar el paciente"})
    }
}

//Recupera los datos del paciente en la base de datos
export const traerPaciente = async(req,res) => {

    //Esto es para el seguimiento en caso de error 
    const ACCION = "traerPaciente"

    try {
        
        //Recupera el paciente
        const paciente = req.paciente

        //Retorna los datos del paciente
        return res.status(200).send(paciente)

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error al consultar el paciente"}) 
    }
}

//Edita el paciente 
export const editarPaciente = async(req,res) => {

    //Esto es para el seguimiento en caso de error 
    const ACCION = "editarPaciente"

    try {

        //Recupera el paciente a editar
        const usuario = req.usuario

        //Trae los nuevos datos 
        const data = req.data

        //Actualiza el paciente
        const actualizarPaciente = await Paciente.update(data, {
            where : {
                id : usuario
            }
        })

        //Revisa si se editó el paciente
        if(actualizarPaciente != null){
            return res.status(200).json({Message : "Se editó el paciente"})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error al editar el paciente"}) 
    }
}

//Crea un nuevo usuario y un nuevo paciente
export const crearUsuarioPaciente = async(req,res) => {


    //Esto es para el seguimiento en caso de error 
    const ACCION = "crearUsuarioPaciente"

    try{

        //Recupera los datos del usuario
        const data = req.user_data

        //Recupera los datos de paciente
        const pacienteData = req.paciente_data

        //Recupera la contraseña aleatoria generada
        const emailPass = req.emailPass

        //Genera una transacción para la creación del usuario y paciente
        await sequelize.transaction(async(t) => {

            //Crea un nuevo usuario
            const usuarioNuevo = await Usuario.create(data, {transaction : t})

            //¿Se creo el usuario?
            if(usuarioNuevo != null){

                 //Trae la id del usuario que se creo
                const idUsuario = usuarioNuevo.id
      
                //Agrega la id del usuario a las propiedades del paciente
                pacienteData.UserID = idUsuario 

                //Crea un nuevo paciente
                const pacienteNuevo = await Paciente.create(pacienteData, {transaction : t})
                
                //Busca el medico responsable del paciente
                const medico = await Medico.findByPk(req.medico_encargado)
    
                //Asigna el paciente al médico
                await pacienteNuevo.addDoctor(medico, {transaction : t})

                //Esta parte envia un correo al paciente, pero esta por definir la interacción.
                if(pacienteNuevo != null){
    
                    // return res.status(200).json({Message : "Se creo exitosamente el paciente"}) 

                    const html = fs.readFileSync('src/mailer/pacienteUsuario.html', 'utf-8');
    
                    const template = handlebars.compile(html)
                    
                    const datosCorreoRegistro = {
                        "nombre_completo": usuarioNuevo.name + " " + usuarioNuevo.last_name,
                        "contrasena" : emailPass
                    }
                    
                    const htmlAEnviar = template(datosCorreoRegistro)
            
                    const email = await enviarEmail(usuarioNuevo.email,"Registro en FonoDX", htmlAEnviar)
            
                    if(email == "enviado"){
                        return res.status(200).json({Message : "Se creo exitosamente el paciente"}) 
                    } else {
                        return res.status(500).json({Error : "Al parecer ocurrio un error en su registro, intentelo más tarde."})
                    }
                }
    
            }
        })

    }catch(error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error al registrar el paciente"})
    }
}

//Recupera datos del paciente
export const buscarPaciente = async(req,res) => {   

    //Esto es para el seguimiento en caso de error 
    const ACCION = "buscarPaciente"

    try{

        //Recupera los datos del paciente
        const data = req.patient_data

        //Retorna los datos del paciente
        return res.status(200).json(data)

    }catch (error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error recuperando el paciente."})
    }
}

//Enviar resultados por correo
export const enviarResultados = async (req,res) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "enviarResultados"

    try {

        const auscultacion = req.auscultacion
        const correo =  req.correo

        const consultarResultados = await Resultado.findOne({
            include : [{
                model : Senal
            }], 
            where : {
                SignalID : auscultacion
            }
        })

        if(consultarResultados != null){

            const {diagnostic,disease,tp,tn,fp,fn,vpp,vpn,specificity,sensivility,createdAt,signal} = consultarResultados.dataValues

            //Lee plantilla html de invitación
            const html = fs.readFileSync('src/mailer/correoResultados.html', 'utf-8');
    
    
            //Compila la plantilla
            const template = handlebars.compile(html)
                
            //Carga los datos del correo
            const datosCorreoRegistro = {
                    "diagnostico" : diagnostic,
                    "enfermedad" : disease != null || disease != "" ? "Ninguna" : disease.toUpperCase(),
                    "tp" : tp,
                    "tn": tn,
                    "fp" : fp,
                    "fn" : fn,
                    "sensibilidad" : sensivility,
                    "especificidad" : specificity,
                    "vpp" : vpp,
                    "vpn" : vpn,
                    "fechaauscultacion" : DateTime.fromJSDate(createdAt).setZone('UTC').setZone('America/Bogota').toFormat('yyyy-MM-dd HH:mm'),
                    "fecharesultados" : DateTime.fromJSDate(signal.dataValues.createdAt).setZone('UTC').setZone('America/Bogota').toFormat('yyyy-MM-dd HH:mm'),
            }

            //Compila la plantilla con los datos
            const htmlAEnviar = template(datosCorreoRegistro)
        
            //Envia el correo
            const email = await enviarEmail(correo,"Resultados auscultación", htmlAEnviar)
    
            if(email == "enviado"){
                return res.status(200).json({Message : "Se enviaron los resultados exitosamente"}) 
            } else {
                //Cancela la transacción
                t.rollback();
                return res.status(500).json({Error : "Al parecer ocurrio un error enviando los resultados, intentelo más tarde."})
            }
            
        }else{
             return res.status(404).json({Error: "Esta auscultación no tiene resultados"})
        }


    }catch(error){
        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error en el servidor"}) 
    }
}