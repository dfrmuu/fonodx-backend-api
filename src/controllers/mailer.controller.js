import { Usuario } from "../models/Usuario.js";
import * as nodemailer from 'nodemailer'
import {config} from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
config()
import * as fs from 'fs'
import { registrarLogError } from "./errorlog.controller.js";

const RECURSO = "correos"

//Enviar correos desde el aplicativo
export const enviarEmail = async (correo,subject,html) => {

    const ACCION = "enviarEmail"

    try{
        //Cuenta de GMAIL
        let cuentaEmail = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, 
            auth: {
                user: process.env.GMAIL_USER,
                pass: process.env.GMAIL_PASS, 
            },
        });

        // Envia correo
        await cuentaEmail.sendMail({
            from: '"Mailer (FONODX)" <emailertest674@gmail.com>', //¿Quien lo envia?
            to: correo, // ¿Quien lo recibe?
            subject: subject, // Asunto
            html: html, // Cuerpo del mensaje
        });

        return "enviado"

    }catch(error){

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)
    }
}