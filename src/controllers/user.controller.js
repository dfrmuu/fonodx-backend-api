import {config} from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { sequelize } from "../database/database.js"
import { Rol } from "../models/Rol.js"
import { Administrativo } from '../models/Administrativo.js'
import { Medico } from '../models/Medico.js'
import { Paciente } from "../models/Paciente.js"
import {  enviarEmail } from '../controllers/mailer.controller.js'
import { Usuario } from "../models/Usuario.js"
import * as fs from 'fs'
import bcrypt from "bcrypt";
import handlebars  from 'handlebars'
import { Op } from "sequelize"
import { Investigador } from "../models/Investigador.js"
import { registrarLogError } from "./errorlog.controller.js"


/*
 * Proyecto: FonoDX
 * Controlador: Usuarios
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Operaciones tabla 'users'.
 */

//Carga de variables de entorno
config() 

const RECURSO = "usuarios"

//Recupera los usuarios de la base de datos
export const traerUsuarios = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerUsuarios"

    try {

        //Trae todos los usuarios
        const usuarios = await Usuario.findAll({
            include : [{
                model : Rol,
            }],
            attributes : ['id','name','ci','last_name','RoleID','email','name_user','status', 'phone','RoleID']
        })

        //Envia los usuarios
        return res.status(200).send(usuarios)

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error"})
    }
}

//Trae el usuario
export const traerUsuario = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerUsuario"

    try {
        
        //Recupera los datos en la petición
        const usuario = req.usuario

        //Envia los datos del usuario
        return res.status(200).send(usuario)

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error"})
    }
}

//Crea un usuario nuevo
export const crearUsuario = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "crearUsuario"

    try {

        //Recupera los datos en la petición
        const data = req.data
        const contrasena = req.contrasena
        const dataRol = req.dataRol
        const rol = req.data.RoleID

        var nombreRol 


        //Crear una transacción 
        await sequelize.transaction( async (t) => {

            //Crea un usuario
            const crearUsuario = await Usuario.create(data, {transaction : t})

            if(crearUsuario != null){

                //Según el rol
                switch (rol) {

                    case 1:
                        //Asigna la id del nuevo usuario al objeto de rol
                        dataRol.UserID = crearUsuario.id

                        //Crea un nuevo registro según el rol
                        await Paciente.create(dataRol, {transaction : t})
                        nombreRol = "PACIENTE"
                    break;
    
                    case 2:
                        //Asigna la id del nuevo usuario al objeto de rol      
                        dataRol.UserID = crearUsuario.id

                        //Crea un nuevo registro según el rol
                        await Medico.create(dataRol, {transaction : t})
                        nombreRol = "MEDICO"
                    break;
    
                    case 3:
                        //Asigna la id del nuevo usuario al objeto de rol   
                        dataRol.UserID = crearUsuario.id

                        //Crea un nuevo registro según el rol
                        await Investigador.create(dataRol, {transaction : t})
                        nombreRol = "INVESTIGADOR"
                    break;


                    case 4:
                        //Asigna la id del nuevo usuario al objeto de rol      
                        dataRol.UserID = crearUsuario.id

                        //Crea un nuevo registro según el rol
                        await Administrativo.create(dataRol, {transaction : t})
                        nombreRol = "ADMINISTRADOR"
                    break;
                }
    
                //Carga la plantilla de registro de usuario
                const html = fs.readFileSync('src/mailer/registroUsuario.html', 'utf-8');
    
                //Lee la plantilla 
                const template = handlebars.compile(html)

                //Agrega los datos a la plantilla
                const datosCorreoRegistro = {
                    "nombre_completo": data.name + " " + data.last_name,
                    "rol" : nombreRol,
                    "usuario" : data.email,
                    "clave" :contrasena
                }

                //Carga los datos en la plantilla
                const htmlAEnviar = template(datosCorreoRegistro)
    
                //Envia el correo avisando el registro del usuario
                await enviarEmail(
                    data.email,
                    "Registro en FonoDX",
                    htmlAEnviar
                )
            }  
        })

        return res.status(200).json({Message : "Se agregó el usuario."})

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error en el servidor"})        
    }
}

//Desactiva un usuario
export const desactivarUsuario = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "desactivarUsuario"

    try {

        //Recupera los datos del usuario
        const usuario = req.usuario

        //Cambia el estado del usuario
        usuario.status = "inactivo"

        //Guarda la información
        await usuario.save()

        return res.status(200).json({Message : "Se desactivó el usuario."})

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en la base de datos"})        
    }
}

//Cambia el estado de un usuario a activo
export const activarUsuario = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "activarUsuario"

    try {

        //Recupera los datos del usuario
        const usuario = req.usuario

        //Cambia el estado del usuario
        usuario.status = "activo"

        //Guarda la información
        await usuario.save()

        return res.status(200).json({Message : "Se activó el usuario."})

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en la base de datos"})        
    }
}

//Trae los datos de la persona que haya iniciado sesión
export const traerMiUsuario = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerMiUsuario"

    try {
        
        //Recupera los datos del usuario
        const usuario = req.usuario

        return res.status(200).send(usuario)

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en la base de datos"})       
    }
}

//Edita los datos del usuario que haya iniciado sesión
export const editarMiUsuario = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "editarMiUsuario"

    try {
        
        //Recupera los datos en la petición
        const usuario = req.usuario
        const data = req.data

        //Actualiza los datos del usuario
        const actualizarUsuario = await Usuario.update(data, {
            where :{
                id : usuario
            }
        })

        if(actualizarUsuario){
            return res.status(200).json({Message : "Se actualizó el usuario."})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en la base de datos"})   
    }
}

//Edita los datos de un usuario
export const editarUsuario = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "editarUsuario"

    try {

        //Recupera los datos en la petición
        const usuario = req.usuario
        const data = req.data
        const job_data = req.job_data

        //Crea una transacción
        await sequelize.transaction(async(t) => {

            //Actualiza los datos del usuario
            await Usuario.update(data, {
                where : {
                    id : usuario.id
                }
            })

            //Segun el rol
            switch (usuario.RoleID) {
                case 1:

                    //Busca en el rol según la id del usuario
                    const paciente = await Paciente.findOne({
                        UserID : usuario.id
                    })
    
                    //Actualiza los datos del rol
                    paciente.age = job_data.age
                    paciente.familiar = job_data.familiar
                    paciente.familiar_phone = job_data.familiar_phone

                    //Guarda
                    await paciente.save({transaction : t})

                break;
                case 2:

                    //Busca en el rol según la id del usuario
                    const medico = await Medico.findOne({
                        UserID : usuario.id
                    })
    
                    //Actualiza los datos del rol
                    medico.job_title = job_data.job_title
                    medico.area = job_data.area

                    //Guarda
                    await medico.save({transaction : t})

                break;
                case 3:

                    //Busca en el rol según la id del usuario
                    const investigador = await Investigador.findOne({
                        UserID : usuario.id
                    })
    
                    //Actualiza los datos del rol
                    investigador.job_title = job_data.job_title
                    investigador.area = job_data.area

                    //Guarda
                    await investigador.save({transaction : t})
                break;
                case 4 : 

                    //Busca en el rol según la id del usuario
                    const admin = await Administrativo.findOne({
                        ID_Usuario : usuario.id
                    })
    
                    //Actualiza los datos del rol
                    admin.job_title = job_data.job_title
                    admin.area = job_data.area

                    //Guarda
                    await admin.save({transaction : t})
                break;
            }

            return res.status(200).json({Message : "Se actualizó el usuario"})
        })

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error"})
    }
}

//Elimina un usuario
export const eliminarUsuario = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "eliminarUsuario"

    try {
        
        //Recupera los datos en la petición
        const usuario = req.usuario

        //Elimina el usuario
        await Usuario.destroy({
            where : {
                id : usuario.id
            }
        })

        return res.status(200).json({Message : "Se eliminó el usuario"})

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error"})  
    }
}

//Filtra datos 
export const filtrarDatos = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "filtrarDatos"

    try {

        //Recupera los datos en la petición
        const dato = req.dato;
        const criterio = req.criterio;
        let whereCondition;

        //Según el tipo de criterio se cambia la condición del where
        if (criterio === "name") {
            whereCondition = sequelize.where(
                sequelize.fn("concat", sequelize.col("users.name"), ' ', sequelize.col("users.last_name")),
                {
                    [Op.like]: `%${dato}%`
                }
            );
        } else {
            whereCondition = {
                [criterio] : {
                    [Op.like]: `%${dato}%`
                }
            };
        } 

        //Recupera los usuarios a partir de los filtros
        const usuarios = await Usuario.findAll({
            include : [{
                model : Rol,
            }],
            where : whereCondition,
            attributes : ['id','name','ci','last_name','RoleID','email','name_user','status', 'phone']
        })
        
        //¿Hay resultados?
        if(usuarios.length == 0){
            return res.status(404).json({Error : "No hay datos según los parametros enviados."})
        }else{
            return res.status(200).send(usuarios);
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({ Error: "Ocurrió un error al filtrar los usuarios." });
    }
}

//Revisa si la clave enviada por el usuario es la misma que el usa
export const revisionClave = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "revisionClave"

    try {
        
        //Recupera los datos en la petición
        const valido = req.valido

        //¿Es válido?
        if(valido){
            return res.status(200).json({valido : true})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({ Error: "Ocurrió un error al filtrar los usuarios." });
    }
}

//Cambia la clave de un usuario
export const cambioClave = async(req,res) => {


    //Esto es para agregar el origen del error en el log
    const ACCION = "cambioClave"

    try {
        
        //Recupera los datos en la petición
        const clave = req.clave
        const usuario = req.usuario

        //Hashea la clave (10 salt)
        const claveNuevaHasheada = await bcrypt.hash(clave,10)

        //Cambia la clave del usuario
        usuario.password = claveNuevaHasheada

        //Guarda los datos
        const claveNueva = await usuario.save()

        //¿Se cambió la clave?
        if(claveNueva != null){
            return res.status(200).json({Message : "Se cambio la clave exitosamente."})
        }

    } catch (error) {
        
        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({ Error: "Ocurrió un error al filtrar los usuarios." });
    }
}