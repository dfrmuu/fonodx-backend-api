import { Rol } from '../models/Rol.js'
import { Permiso } from '../models/Permiso.js'
import { DateTime } from 'luxon'
import { registrarLogError } from './errorlog.controller.js'

/*
 * Proyecto: FonoDX
 * Controlador: Roles
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Operaciones en tabla "roles"
 */


const RECURSO = "roles"

//Recupera los roles del sistema
export const traerRoles = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerRoles"

    try {

        //Trae los roles
        const listaRoles = await Rol.findAll()

        //¿Hay roles?
        if(listaRoles.length == 0){
            return res.status(404).json({Error : "No hay datos registrados"})
        }else{
            return res.status(200).send(listaRoles)

        }
    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error en el servidor"})
    }
}

//Trae los permisos de un rol
export const traerPermisos = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "traerPermisos"

    try {

        //Recupera el rol en la petición
        const rol = req.rol

        //Trae los permisos de un rol
        const permisos = await Permiso.findAll({
            where : {
                RoleID : rol
            }
        })

        //¿El rol tiene permisos?
        if(permisos.length == 0){
            return res.status(404).json({Error : "No hay permisos asignados"})
        }else{
            return res.status(200).send(permisos)
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error en el servidor"})
    }
}

//Agrega un permiso a un rol
export const agregarPermiso = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "agregarPermiso"

    try {
        
        //Recupera los datos en la petición
        const data = req.data

        //Crea el permiso
        const añadirPermiso = await Permiso.create(data)

        return res.status(200).json({Message: "Se agrego el permiso"})

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error en el servidor"})
    }
}

//Quita un permiso de un rol
export const quitarPermiso = async(req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "quitarPermiso"

    try {
        
        //Recupera la id del permiso
        const permiso = req.permiso

        //Elimina el permiso
        const quitarPermisos = await Permiso.destroy({
            where :{
                id : permiso
            }
        })

        return res.status(200).json({Message: "Se quitó el permiso"})

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error: "Ocurrio un error en el servidor"})
    }
}