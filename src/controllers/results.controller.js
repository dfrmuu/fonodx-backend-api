import { Paciente } from "../models/Paciente.js"
import { Resultado } from "../models/Resultado.js"
import { Senal } from '../models/Senal.js'
import { Usuario } from '../models/Usuario.js'
import { registrarLogError } from "./errorlog.controller.js"

/*
 * Proyecto: FonoDX
 * Controlador: Resultados
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Operaciones tabla "results".
 */


const RECURSO = "resultados"

//Recupera los resultados de una auscultación
export const traerResultadoSenal = async (req,res) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarResultadosSenal"

    try {

        const senal = req.senal

        const datosResultados = await Resultado.findAll({
            include : [{
                model : Senal,
                include : [{
                    model : Paciente,
                    include : [{
                        model : Usuario
                    }]
                }]
            }],
            where : {
                SignalID : senal
            }
        })

        if(datosResultados.length == 0){
            return res.status(404).json({Error : "Actualmente esta señal no tiene resultados."})
        }else{
            return res.status(200).send(datosResultados)
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error"})
    }
}
