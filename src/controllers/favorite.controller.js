import { Op, Sequelize } from 'sequelize'
import { Favorito } from '../models/Favorito.js'
import { Medico } from '../models/Medico.js'
import { Senal } from '../models/Senal.js'
import { registrarLogError } from './errorlog.controller.js'

/*
 * Proyecto: FonoDX
 * Controlador: Favoritos (señales de referencia)
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 03
 * Descripción: Validación / agrupamiento de datos.
 */

const RECURSO = "favoritos/referencias"


//Recupera los favorito de la base de datos
export const misFavoritos = async(req,res) => {

    const ACCION  = "misFavoritos"

    try {
        
        //Recupera los datos en la petición
        const medico = req.medico

        //Trae los favoritos / señales de referencia 
        const favoritos = await medico.getFavorite_signals()

        return res.status(200).send(favoritos)


    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)

        return res.status(500).json({Error : "Ocurrio un error en el servidor al recuperar sus favoritos"})
    }
}

//Crea un favorito / señal de referencia en la base de datos
export const crearFavorito = async (req,res) => {

    const ACCION  = "misFavoritos"

    try {

        //Recupera los datos en la petición
        const data = req.form_data

        //Crea el registro
        const nuevoFavorito = await Favorito.create(data)

        if(nuevoFavorito != null){
            return res.status(200).json({Message : "Se guardo su favorito."})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)


        return res.status(500).json({Error : "Ocurrio un error en el servidor al guardar sus favoritos"})
    }
}

//Edita los datos de un favorito / señal de referencia en la base de datos
export const editarFavorito = async (req,res) => {

    const ACCION  = "editarFavorito"

    try {

        //Recupera los datos en la petición
        const data = req.data
        const favorito = req.favorito

        //Edita la señal
        const señalEditada = await Favorito.update(data,
            {
                where : {
                    id : favorito
                }
            }
        )

        if(señalEditada != null){
            return res.status(200).json({Message : "Se edito su favorito"})
        }
        
    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)


        return res.status(500).json({Error : "Ocurrio un error en el servidor al editar su favorito"})  
    }
}

//Elimina un favorito / señal de referencia de la base de datos
export const eliminarFavorito = async(req,res) => {

    const ACCION  = "eliminarFavorito"

    try {

        //Recupera los datos de la petición
        const favorito = req.favorito
        
        //Borra el registro
        const eliminarSeñal = await Favorito.destroy({
            where : {
                id : favorito
            }    
        })

        if(eliminarSeñal != null){
            return res.status(200).json({Message : "Se eliminó la referencia."})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)
        
        return res.status(500).json({Error : "Ocurrio un error en el servidor al editar su favorito"})   
    }
}

//Filtra los favoritos del médico
export const filtrarFavorito = async(req,res) => {
    
    const ACCION  = "filtrarFavorito"

    try {

        //Recupera los datos de la petición
        const dato = req.dato
        const criterio = req.criterio
        const medico = req.medico
        var whereCondition 

        //Según el criterio ajusta los parametros de busqueda
        if(criterio == "notes"){
            whereCondition = {
                notes : {
                    [Op.like] : `%${dato}%`
                },
            }
        } else {
            whereCondition = {
                createdAt : {
                    [Sequelize.Op.between]: [
                        `${dato} 00:00:00`,
                        `${dato} 23:59:59`
                    ] 
                },
            }
        }


        //Trae los favoritos / señales de referencia 
        const favoritosFiltrados = await medico.getFavorite_signals({
            through : {
                where : whereCondition
            }
        })



        if(favoritosFiltrados.length > 0){
            return res.status(200).send(favoritosFiltrados)
        } else {
            return res.status(404).json({Error : "No hay datos según sus criterios de busqueda."})
        }

    } catch (error) {

        //Se arma el objeto para el registro en el log
        const dataError = { 
            resource : RECURSO,
            action : ACCION,
            info : error.message
        }

        const registroError = await registrarLogError(dataError)
        
        return res.status(500).json({Error : "Ocurrio un error en el servidor al editar su favorito"})   
    }
}

//Recupera las fechas de los favoritos
export const recuperarFechasFavoritos = async(req,res) => {

    const ACCION  = "recuperarFechasFavoritos"

    try {

        const medico = req.medico

        //Consulta los favoritos del médico
        const fechasFavoritos = await Favorito.findAll({
          attributes: [
            [
              Sequelize.literal('DATE_FORMAT(createdAt, "%Y-%m-%d")'),
              'createdAt', // Truncamos la fecha a "Y-m-d"
            ],
        ],
        where: {
          doctorId: medico,
        },
          group: [Sequelize.literal('DATE_FORMAT(createdAt, "%Y-%m-%d")')], // Agrupamos por "Y-m-d"
          order: [['createdAt', 'DESC']],
        });

        if(fechasFavoritos.length > 0 ){
          return res.status(200).send(fechasFavoritos)
        }

    } catch (error) {

          //Se arma el objeto para el registro en el log
          const dataError = { 
              resource : RECURSO,
              action : ACCION,
              info : error.message
          }

          const registroError = await registrarLogError(dataError)

          return res.status(500).json({Error : "Ocurrio un error"})     
    }
}