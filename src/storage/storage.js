import multer from "multer";
import {config} from 'dotenv' 
import path from 'path'
import mime  from 'mime-types'

/*
 * Proyecto: FonoDX
 * Almacenamiento: Auscultaciones
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 08
 * Descripción: Aquí se mantiene la ruta de almacenamiento de las auscultaciones.
 */ 

//Carga las variables de entorno
config()

//Trae la ruta de almacenamiento
const rutaAlmacenamiento = process.env.URL_SIGNALS

export const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      // cb(null, '//192.168.43.181/Repositorio')
      cb(null, rutaAlmacenamiento)
    },
    filename: function (req, file, cb) {
      try{
          cb(null, file.originalname + '.' + mime.extension(file.mimetype))
      }catch(error){
        console.log(error)
      }
    }
})