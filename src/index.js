import app from "./app.js";
import * as dotenv from 'dotenv'
//Inicializa las variables de entorno
dotenv.config()
import {sequelize} from "./database/database.js"
import "./models/Rol.js";
import "./models/Medico.js";
import "./models/Paciente.js"
import "./models/Favorito.js"
import "./models/Senal.js"
import "./models/Usuario.js";
import "./models/Administrativo.js"
import "./models/Investigador.js"
import "./models/TokenContrasena.js"
import "./models/Actividad.js"
import "./models/Tarea.js"
import "./models/Permiso.js"
import "./models/Resultado.js"
import "./models/ErrorLog.js"
import "./models/LoginLog.js"
import "./associations/associations.js"

/*
 * Proyecto: FonoDX
 * Iniciar el servidor de la API 
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Se encarga de iniciar el servidor del API y sincronizar la base de datos.
 */ 


async function main(){
    
    try {

        //Toma el puerto
        const port = process.env.PORT || 8080;
        app.listen(port)

        //Se conecta con la base de datos 
        //Fuerza la sincronización, limpia todos los registros y vuelve a estructurar la base de datos
        // await sequelize.sync({force:true});
        await sequelize.sync();
        console.log(`Server running in ${process.env.PORT}`);
        console.log("Se conecto a la base de datos satisfactoriamente");
    } catch (error) {
        console.error("Ocurrio un error conectandose a la base de datos",error) 
    }
}
 
main()