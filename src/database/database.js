import { Sequelize } from "sequelize";
import {config} from 'dotenv' 

/*
 * Proyecto: FonoDX
 * Inicializar base de datos
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Se encarga de gestionar las credenciales de la base de datos
 */ 

config()

const PRODUCTION = "production"

//Hace la conexión con la base de datos
export const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
        host: process.env.NODE_ENV == PRODUCTION ? `mysql.vitalsignsproject.com` : 'mysql.vitalsignsproject.com',
        dialect:  'mysql',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },
}); 

 