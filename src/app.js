import {config} from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import express from "express";
import bodyParser from "body-parser";
import rutasRoles from './routes/role.routes.js'
import rutasUsuarios from './routes/user.routes.js'
import rutasSeñales from './routes/sign.routes.js'
import rutasAutentificacion from './routes/auth.routes.js'
import rutasInvitacion from './routes/invitation.routes.js'
import rutasPacientes from './routes/patient.routes.js'
import rutasFavoritos from './routes/favorite.routes.js'
import rutasActividades from './routes/activity.routes.js'
import rutasResultados from './routes/result.router.js'
import rutasTareas from './routes/toDo.routes.js'
import rutasLoginLog from './routes/loginlog.routes.js'
import rutasErrorLog from './routes/errorlog.routes.js'
import cors from "cors";
import multer from "multer";

/*
 * Proyecto: FonoDX
 * Punto de entrada principal
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Punto de entrada donde permite la gestión de rutas y peticiones.
 */ 

//Nos permite usar las variables de entorno
config()

//Url front-end
const urlFront = process.env.URL_FRONT

//Url auscultaciones
const urlAuscultaciones = process.env.URL_SIGNALS

//Url imagenes-gifs
const urlImgs = process.env.URL_IMGS

//Crea una instancia de Express, que se utilizará para definir rutas y configurar el servidor.
const app = express();

//Se utiliza para analizar datos JSON de las solicitudes entrantes.
const jsonParser = bodyParser.json()

app.use(bodyParser.urlencoded({
    extended: false
}));

//Configura el Compartir recursos entre orígenes (CORS). Permite solicitudes desde el front e incluye credenciales en las solicitudes.
app.use(cors({
    origin: urlFront,
    credentials: true
}));


//Rutas de inicio API
app.get('/', (req,res) => res.send('Estas conectado!') );
// app.use('/recursos', express.static('//192.168.43.181/Repositorio'));

//Ruta estatica para recuperación de archivos
app.use('/recursos', express.static(urlAuscultaciones));

//Ruta estatica para recuperación de archivos (imagenes-gifs)
app.use('/images', express.static(urlImgs));


//Rutas del aplicativo
app.use(jsonParser);
app.use(rutasRoles);
app.use(rutasUsuarios);
app.use(rutasAutentificacion);
app.use(rutasActividades)
app.use(rutasFavoritos);
app.use(rutasInvitacion);
app.use(rutasPacientes);
app.use(rutasTareas);
app.use(rutasSeñales);
app.use(rutasLoginLog);
app.use(rutasErrorLog);
app.use(rutasResultados);


export default app;