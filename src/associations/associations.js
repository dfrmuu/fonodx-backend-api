import { Favorito } from "../models/Favorito.js";
import { Medico } from "../models/Medico.js";
import { Paciente } from "../models/Paciente.js";
import { Senal } from "../models/Senal.js";
import { Usuario } from "../models/Usuario.js";
import { Invitacion } from "../models/Invitacion.js";
import { Administrativo } from "../models/Administrativo.js";
import { Actividad } from "../models/Actividad.js";
import { Tarea } from "../models/Tarea.js";
import { Rol } from "../models/Rol.js";
import { TokenContrasena } from '../models/TokenContrasena.js'
import { Permiso } from "../models/Permiso.js";
import { Resultado } from "../models/Resultado.js"
import { ErrorLog } from '../models/ErrorLog.js'
import { LoginLog } from '../models/LoginLog.js'
import { Investigador } from "../models/Investigador.js";


/*
 * Proyecto: FonoDX
 * Archivo de relaciones
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 03
 * Descripción: Aquí se gestionan las relaciones de las tablas en la base de datos.
 */


//Relacion usuario y rol.
Rol.hasOne(Usuario, {   //Un usuario tiene un rol, referenciado en usuario por 'ID_Rol'
    foreignKey: 'RoleID',
    sourceKey: 'id'
})

Usuario.belongsTo(Rol, {  //Un rol pertenece a un usuario.
    foreignKey: 'RoleID',
    targetKey: 'id'
})


//Relacion permiso - rol
Permiso.belongsTo(Rol, { //One to many (uno a muchos) - Permisos - Rol
    foreignKey: 'RoleID',
    targetKey: 'id'
})

Rol.hasMany(Permiso, {
    foreignKey : "RoleID"
})


//Relacion usuario - paciente
Paciente.belongsTo(Usuario, {  //One to One (uno a uno) - Para ser paciente tiene que estar en la tabla de usuarios.
    foreignKey: 'UserID',
    targetKey: 'id'
})


Usuario.hasOne(Paciente, {    //One to One (uno a uno) -Usuario puede tener solamente una ID de paciente
    foreignKey: 'UserID',
    targetKey: 'id'
})


//Relacion usuario - médico
Usuario.hasOne(Medico, { //One to One (uno a uno) - Un usuario puede ser medico
    foreignKey: 'UserID',
    targetKey: 'id'
})

Medico.belongsTo(Usuario, { //One to One (uno a uno) - Un rol de medico puede pertenecer a un usuario
    foreignKey: 'UserID',
    targetKey: 'id'
})
    
//Relacion usuario - administrativo
Usuario.hasOne(Administrativo, { //One to One (uno a uno) - Un usuario puede ser medico
    foreignKey: 'UserID',
    targetKey: 'id'
})

Administrativo.belongsTo(Usuario, { //One to One (uno a uno) - Un rol de medico puede pertenecer a un usuario
    foreignKey: 'UserID',
    targetKey: 'id'
})

//Relacion usuario - investigador
Usuario.hasOne(Investigador, { //One to One (uno a uno) - Un usuario puede ser medico
    foreignKey: 'UserID',
    targetKey: 'id'
})

Investigador.belongsTo(Usuario, { //One to One (uno a uno) - Un rol de investigador puede pertenecer a un usuario
    foreignKey: 'UserID',
    targetKey: 'id'
})


//Relacion paciente - auscultacion
Senal.belongsTo(Paciente, {   
    foreignKey: 'PatientID',
    targetKey: 'id'
})

//Relación médico - pacientes
Medico.belongsToMany(Paciente, {   //Many to Many (muchos a muchos) - Pacientes - Medicos
    through : "doctors_patients",
    as : "patients",
})

Paciente.belongsToMany(Medico, {   
    through : "doctors_patients",
    as : "doctors",     
}) 


//Relación auscultaciones - médico
Medico.belongsToMany(Senal, {   //Many to Many (Señales - Medicos)
    through : Favorito,
    as : "favorite_signals",
})

Senal.belongsToMany(Medico, {   
    through : Favorito,
    as : "favorite_signals",     
}) 

 
//Relación invitaciones - administrativo
Invitacion.belongsTo(Administrativo, { //One to many (uno a muchos) - Invitaciones - Usuario
    foreignKey: 'AdminID',
    targetKey: 'id'
})

//Relación usuario - actividad
Usuario.hasMany(Actividad, {
    foreignKey : "UserID",
    targetKey : 'id',
    onDelete : 'CASCADE'
})


Actividad.belongsTo(Usuario, {
    foreignKey : "UserID",
    targetKey : 'id'
})

//Relación usuario - tarea
Tarea.belongsTo(Usuario, { //One to many (uno a muchos) - Invitaciones - Usuario
    foreignKey: 'UserID',
    targetKey: 'id'
})

Usuario.hasMany(Tarea, {
    foreignKey : "UserID",
    onDelete : 'CASCADE'
})

//Relación usuario - tokens contraseña
Usuario.belongsToMany(TokenContrasena, {   //Many to Many (Tokens - usuarios)
    through : 'tokens_users',
    as : "tokens",
})

TokenContrasena.belongsToMany(Usuario, {   
    through : 'tokens_users',
    as : "users",     
}) 

//Relación auscultacion / señal - resultados
Resultado.belongsTo(Senal, { //One to many (uno a muchos) - Invitaciones - Usuario
    foreignKey: 'SignalID',
    targetKey: 'id'
})

Senal.hasMany(Resultado, {
    foreignKey : "SignalID",
    targetKey : 'id'
})

//Relación logs inicio de sesión / usuarios
LoginLog.belongsTo(Usuario, { //One to many (uno a muchos) - LoginLogs - Usuario
    foreignKey: 'UserID',
    targetKey: 'id'
})

Usuario.hasMany(LoginLog, {
    foreignKey : "UserID",
    targetKey : 'id',
    onDelete : 'CASCADE'
})


