import { Permiso } from "../models/Permiso.js";

/*
 * Proyecto: FonoDX
 * Middleware : Permisos
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Revisión de permisos de usuario.
 */


//Valida los permisos en la petición
export const validarPermiso = async(req,res,next) => {
    try {
        
        //Revisa si vienen los permisos en la cabecera de la petición
        if(!req.header('Resource') && !req.header('Action')){
            return res.status(401).json({Error : "Servicio no disponible, intentelo más tarde."})
        }

        //Cabeceras de la petición
        const recurso = req.header('Resource')
        const accion = req.header('Action')

        //Revisa la existencia del permiso en el rol
        const existePermiso = await Permiso.findOne({
            where : {
                resource : recurso,
                action : accion,
                RoleID : req.user.RoleID
            }
        })

        if(existePermiso == null){
            return res.status(401).json({Error: "No tiene permisos para esta acción"})
        }else{
            next()
        }

    } catch (error) {
        return res.status(404).json({Error: "Error al validar los permisos, intentelo más tarde."}) 
    }
}