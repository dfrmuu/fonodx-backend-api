import {config} from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
config()

import jwt from 'jsonwebtoken';


/*
 * Proyecto: FonoDX
 * Middleware : Tokens
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Revisión de permisos de usuario.
 */


//Revisa los tokens que se envia en la petición
export const revisarToken = async (req,res,next) => {

    //Trae tanto el token como la clave de cliente  
    const token = req.header('Authorization').split(' ')[1]; //Esto es para quitar el 'Bearer' al inicio del token
    const cliente = req.header('ClientAuth') 

    //Valida si existen los dos valores
    if(token && cliente){
          try{

            // Verifica el token usando jwt.verfiy 
            const decode = jwt.verify(token, process.env.JWT_SECRET_KEY);

            //¿Es valido?.
            if(cliente == process.env.APIKEY){
                req.token_status = "valido"
                req.user = decode


                next()
            } else {
                return res.status(401).json({Error : "Acceso invalido."})
            }
          }catch(error){
              console.log(error)
              return res.status(401).json({Error:"Token expirado o invalido!"})
          }
    }else{
       return res.status(401).json({Error:"No hay token de acceso!"})
    }
};