import {config} from 'dotenv' 
config()


/*
 * Proyecto: FonoDX
 * Middleware : Cliente
 * Desarrollado por:  Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Revisión de la llave de cliente.
 */


//Revisa si la petición tiene la clave de cliente
export const revisarCliente = async (req,res,next) => {

    //Trae la clave de la cabecera de la petición
    const cliente = req.header('ClientAuth')

    if(cliente){
        if(cliente == process.env.APIKEY){
            req.token_status = "valido"
            next()
        } else {
            return res.status(401).json({Error : "Acceso invalido."})
        }
    }else{
       return res.status(401).json({Error:"Este es un acceso invalido!"})
    }
};