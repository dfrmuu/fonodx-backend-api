import { registrarLogError } from "../controllers/errorlog.controller.js";
import { Paciente } from "../models/Paciente.js";
import { Senal } from "../models/Senal.js";

/*
 * Proyecto: FonoDX
 * Capa de negocio: Auscultaciones / señales
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Validación / agrupamiento de datos.
 */

const VALID_TYPE = "valido"
const RECURSO = "auscultaciones"

export const validarSenales = async (req, res, next) => {
    // Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarSenales";

    // Revisa si el token es válido
    if (req.token_status == VALID_TYPE) {
        try {
            // Recupera la id del paciente
            let idPaciente = req.body.idPaciente;


            // Si no se proporciona idPaciente en req.body, se toma de req.user.IdPaciente
            if (!idPaciente && req.user.IdPaciente) {
                idPaciente = req.user.IdPaciente;
            }

            // Consulta si el paciente está en la base de datos
            const pacienteExiste = await Paciente.findOne({
                where: {
                    id: idPaciente,
                },
            });

            // ¿El paciente existe?
            if (pacienteExiste == null) {
                return res.status(404).json({ Error: "El paciente no está en la base de datos." });
            } else {
                // Envía la id del paciente
                req.paciente_id = idPaciente;
                next();
            }
        } catch (error) {
            // Se arma el objeto para el registro en el log
            const dataError = {
                resource: RECURSO,
                action: ACCION,
                info: error.message,
            };

            const registroError = await registrarLogError(dataError);

            return res.status(500).json({ Error: "Ocurrió un error" });
        }
    }
};


//Valida los datos para recuperar los datos de una auscultación
export const validarTraerSenal = async(req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarTraerSenal"

    //Revisa si el token es válido
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera la id en la petición
            const id = req.params.id

            //Consulta la auscultación
            const consultaSenal = await Senal.findOne({
                where : {
                    id : id
                }
            })

            //¿Existe?
            if(consultaSenal == null){
                return res.status(404).json({Error : "Esta auscultación no existe."})
            }else {

                //Envia la auscultación consultada
                req.auscultacion = consultaSenal
                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida los datos para editar una auscultación
export const validarEditarSenal = async(req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarEditarSenal"

    //Revisa si el token es válido
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos enviados en el formulario para editar
            const {id,focus} = req.body

            //Consulta si la auscultación existe
            const consultaSenal = await Senal.findOne({
                where : {
                    id : id
                }
            })

            //¿Existe?
            if(consultaSenal == null){
                return res.status(404).json({Error : "Esta auscultación no existe."})
            }else {

                //Construye el objeto que se enviara después para editar
                const data = {
                    focus : focus
                }

                //Envia los datos
                req.data = data
                req.auscultacion = consultaSenal.id
                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida los datos para eliminar una auscultación
export const validarEliminarSenal = async(req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarEliminarSenal"

    //Revisa si el token es válido
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera la id en la petición
            const id = req.params.id


            //Consulta la existencia de la auscultación
            const consultaSenal = await Senal.findOne({
                where : {
                    id : id
                }
            })

            //¿Existe?
            if(consultaSenal == null){
                return res.status(404).json({Error : "Esta auscultación no existe."})
            }else {

                //Envia la consulta de la auscultación
                req.auscultacion = consultaSenal.id
                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida los datos para la creación de una auscultación
export const validarRegistroSenal = async(req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarRegistroSenal"

    //Revisa si el token es válido
    if(req.token_status == VALID_TYPE){

        try {

            //Recupera los datos del formulario
            const {PatientID, aortic_focus } = req.body

            //Recupera la referencia (path / ruta) del archivo de audio que se envia
            const auscultacion = req.file
        
            //Consulta la existencia del paciente
            const pacienteExiste = await Paciente.findOne({
                where : {
                    id : PatientID
                }
            })

            //¿Existe?
            if(pacienteExiste == null){
                return res.status(404).json({Error : "El paciente no esta en la base de datos."})
            }else{

                //Construye el objeto que se usara para crear la auscultación
                const data = {
                    PatientID : PatientID,
                    focus : aortic_focus,
                    status : "pendiente",
                }

                //Envia los datos de la auscultación
                req.sign_data = data
                //Envia la ruta y el nombre del archivo que se va a cargar
                req.audio = "http://localhost:8080/recursos/"+  auscultacion.filename

                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Recupera las señales / auscultaciones según criterios
export const validarFiltrarAuscultaciones = async(req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarFiltrarAuscultaciones"

    //Revisa si el token es válido
    if(req.token_status == VALID_TYPE){

        try {

            // Recupera la id del paciente
            let {idPaciente,fecha}= req.body

            // Si no se proporciona idPaciente en req.body, se toma de req.user.IdPaciente
            if (!idPaciente && req.user.IdPaciente) {
                idPaciente = req.user.IdPaciente;
            }

            // Consulta si el paciente está en la base de datos
            const pacienteExiste = await Paciente.findOne({
                where: {
                    id: idPaciente,
                },
            });

            if(pacienteExiste == null){
                return res.status(200).json({Error : "Este paciente no existe."})
            }else{
                req.paciente = pacienteExiste.id
                req.fecha = fecha
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Recupera las fechas disponibles en las auscultaciones para agilizar el proceso de consulta
export const validarRecuperarFechasAuscultaciones = async(req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarRecuperarFechasAuscultaciones"

    //Revisa si el token es válido
    if(req.token_status == VALID_TYPE){

        try {

            // Recupera la id del paciente
            let idPaciente = req.body.idPaciente;

            // Si no se proporciona idPaciente en req.body, se toma de req.user.IdPaciente
            if (!idPaciente && req.user.IdPaciente) {
                idPaciente = req.user.IdPaciente;
            }

            // Consulta si el paciente está en la base de datos
            const pacienteExiste = await Paciente.findOne({
                where: {
                    id: idPaciente,
                },
            });

            if(pacienteExiste == null){
                return res.status(200).json({Error : "Este paciente no existe."})
            }else{
                req.paciente = pacienteExiste.id
                next()
            }

        } catch (error) {
            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}