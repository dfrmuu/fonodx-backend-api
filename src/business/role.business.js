import { Rol } from '../models/Rol.js'
import { Permiso } from '../models/Permiso.js'
import { registrarLogError } from '../controllers/errorlog.controller.js'


/*
 * Proyecto: FonoDX
 * Capa de negocio: Roles
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Validación / agrupamiento de datos.
 */


const VALID_TYPE = "valido"
const RECURSO = "roles"

//Valida los datos para traer los roles del sistema
export const validarTraerRoles = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerRoles"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {
            next()
        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}

//Valida los datos para recuperar los permisos de un rol
export const validarTraerPermisos = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerPermisos"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Trae la id en la petición
            const id = req.params.id


            //Consulta el rol por id
            const consultaRol = await Rol.findOne({
                where : {
                    id :id
                }
            })

            //¿El rol existe?
            if(consultaRol == null){
                return res.status(404).json({Error : "Este rol no existe."})
            }else{

                //Envia el rol por la petición
                req.rol = consultaRol.id
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}

//Valida los datos al momento de agregar un permiso
export const validarAgregarPermiso = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarAgregarPermiso"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos en la petición
            const {resource,rol,action} = req.body

            //Consulta si el permiso ya existe para el rol
            const consultaExistencia = await Permiso.findAll({
                where : {
                    resource : resource,
                    action : action,
                    RoleID : rol
                }
            })

            //¿El permiso ya esta presente para ese rol?
            if(consultaExistencia.length > 0){
                return res.status(400).json({Error: "Este permiso ya esta asignado."})
            }else{

                //Construye objeto para agregar permiso
                const data = {
                    resource: resource,
                    action : action,
                    RoleID : rol
                }

                //Envia los datos por la petición
                req.data = data
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}

//Valida los datos para quitar un permiso
export const validarQuitarPermiso = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarQuitarPermiso"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos en la petición
            const {resource,rol,action} = req.body

            //Consulta la existencia del permiso
            const consultaExistencia = await Permiso.findOne({
                where : {
                    resource : resource,
                    action : action,
                    RoleID : rol
                }
            })

            //¿El permiso no existe?
            if(consultaExistencia.length == 0){
                return res.status(400).json({Error: "Este permiso no existe."})
            }else{

                //Envia los datos en la petición 
                req.permiso = consultaExistencia.id
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}