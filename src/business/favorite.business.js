import { registrarLogError } from '../controllers/errorlog.controller.js'
import {Favorito} from '../models/Favorito.js'
import { Medico } from '../models/Medico.js'


/*
 * Proyecto: FonoDX
 * Capa de negocio: Favoritos (señales de referencia)
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 03
 * Descripción: Validación / agrupamiento de datos.
 */

const VALID_TYPE = "valido"
const RECURSO = "favoritos/referencias"

//Valida los datos para recuperar los favoritos del médico
export const validacionMisFavoritos = async(req,res,next) => {

    const ACCION = "validacionMisFavoritos"

    //¿Es válido el token?
    if(req.token_status == VALID_TYPE){
        try {
            
            //Consulta la existencia del médico
            const consultaMedico = await Medico.findOne({
                where : {
                    id : req.user.IdMedico
                }
            })

            //No existe
            if(consultaMedico == null){
                return res.status(404).json({Error : "El médico no existe"})
            }else{

                //Envia los datos mediante la petición
                req.medico = consultaMedico
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}

//Valida los datos para la creación de un favorito (señal de referencia)
export const validacionFavorito = async(req,res,next) => {

    const ACCION = "validacionFavorito"
    
    //¿Es válido el token?
    if(req.token_status == VALID_TYPE){
        try{
            
            //Recupera los datos para la creación del favorito
            const {SignalID, notes} = req.body

            //Consulta  el médico
            const consultaMedico = await Medico.findOne({
                where : {
                    id : req.user.IdMedico
                }
            })

            //Consulta si esta auscultación ya hace parte de la señales de referencia
            const señalExiste = await Favorito.findOne({
                where : {
                    signalId : SignalID,
                    doctorId : req.user.IdMedico
                }
            })


            if(consultaMedico == null){
                return res.status(404).json({Error : "El médico no existe"})
            } if (señalExiste != null){
                return res.status(403).json({Error : "Esta señal ya hace parte de sus referencias"})         
            }else{

                //Construye el objeto para añadir la señal a favoritos
                const data = {
                    doctorId : req.user.IdMedico,
                    signalId : SignalID,
                    notes : notes,
                }

                //Envia los datos mediante la petición
                req.form_data = data
                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}

//Valida los datos para la edición de una señal de referencia
export const validacionEditarFavorito = async(req,res,next) => {

    const ACCION = "validacionEditarFavorito"

    //¿Es válido el token?
    if(req.token_status == VALID_TYPE){
        try {
            
            //Recupera los datos para la creación del favorito            
            const {FavoriteID, notes} = req.body

            //Consulta la existencia de la señal
            const existeLaSeñal = await Favorito.findOne({
                where : {
                    id : FavoriteID
                }
            })

            //No existe
            if(existeLaSeñal == null){
                return res.status(404).json({Error : "Esta señal no existe."})
            }else{

                //Construye el objeto para la edición de la señal
                const data = {
                    notes : notes
                }

                //Envia los datos mediante la petición
                req.favorito = existeLaSeñal.id
                req.data = data
                next()

            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})     
        }
    }
}

//Valida los datos para eliminar un favorito (señal de referencia)
export const validacionEliminarFavorito = async(req,res,next) => {

    const ACCION = "validacionEliminarFavorito"

    if(req.token_status == VALID_TYPE){
        try {

            //Recupera el id en la petición
            const id = req.params.id

            //Consulta si el favorito ya existe
            const existeElFavorito = await Favorito.findOne({
                where : {
                    id : id
                }
            })

            //No existe
            if(existeElFavorito == null){
                return res.status(404).json({Error : "Esta señal de referencia no existe."})
            }else{

                //Envia los datos mediante la petición
                req.favorito = existeElFavorito.id
                next()

            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})  
        }
    }
}

//Valida los datos para filtrar favoritos
export const validacionFiltrarFavoritos = async(req,res,next)  => {
    const ACCION = "validacionFiltrarFavoritos"

    if(req.token_status == VALID_TYPE){
        try {

                        
            //Trae los datos del formulario
            const {dato, criterio} = req.body 

            //Consulta si el médico existe
            const existeMedico = await Medico.findOne({
                where : {
                    id : req.user.IdMedico
                }
            })

            //¿Existe?
            if(existeMedico == null){
                return res.status(200).json({Error : "Este médico no existe"})
            }else{
                //Envia los datos a través de la petición
                req.dato = dato 
                req.criterio = criterio
                req.medico = existeMedico

                next()
            }

        }catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})  
        }
    }
}

//Valida los datos para recuperar las fechas de los favoritos
export const validarRecuperarFechasFavoritos = async(req,res,next) => {

    const ACCION = "validarRecuperarFechasFavoritos"

    if(req.token_status == VALID_TYPE){
        try {

            //Consulta si el médico existe
            const existeMedico = await Medico.findOne({
                where : {
                    id : req.user.IdMedico
                }
            })

            //¿Existe?
            if(existeMedico == null){
                return res.status(200).json({Error : "Este médico no existe"})
            }else{

                req.medico = existeMedico.id
                next()
            }

        }catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})  
        }
    }
}