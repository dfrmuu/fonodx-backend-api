import { DateTime } from 'luxon'
import { Tarea } from '../models/Tarea.js'
import { Usuario } from '../models/Usuario.js'
import { registrarLogError } from '../controllers/errorlog.controller.js'

/*
 * Proyecto: FonoDX
 * Capa de negocio: Tareas
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Validación / agrupamiento de datos.
 */


const VALID_TYPE = "valido"
const RECURSO = "tareas"

//Valida los datos para traer las tareas de un usuario
export const validarTraerTareas = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerTareas"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera la id en la petición
            const id = req.user.Id

            //¿El usuario existe?
            const existeUsuario = await Usuario.findOne({
                where : {
                    id : id
                }
            })

            //Si existe
            if(existeUsuario != null) {
                next()
            } else {
                return res.status(404).json({Error : "Este usuario no existe."})
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : error})  
        }
    }
}

export const validarTraerTarea = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerTarea"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera la id en la petición
            const id = req.params.id

            //¿La tarea existe?
            const existeTarea = await Tarea.findOne({
                where : {
                    id : id
                }
            })

            //Si existe
            if(existeTarea != null) {

                //Envia los datos en la petición
                req.tarea = existeTarea
                next()
            } else {
                return res.status(404).json({Error : "Este usuario no existe."})
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : error})  
        }
    }
}

//Valida los datos para la creación de tareas
export const validarCrearTareas = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarCrearTareas"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos en la petición
            const {homework,description} = req.body

            //Construye el objeto para crear la tarea
            const dataTarea = {
                homework : homework,
                description : description,
                status : false,
                UserID : req.user.Id
            }

            //Envia los datos mediante la petición 
            req.data = dataTarea
            next()

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : error})  
        }
    }
}

//Valida los datos para la edición de tareas
export const validarEditarTareas = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarEditarTareas"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos en la petición
            const {id,homework,description} = req.body

            //¿Existe la tarea?
            const tareaExiste = await Tarea.findOne({
                where : {
                    id : id
                }
            })


            //No existe
            if(tareaExiste == null){
                return res.status(404).json({Message : "Esta tarea no existe."})
            }else{

                //Construye el objeto que se envia para editar la tarea
                const dataTarea = {
                    homework : homework,
                    description : description
                }
    
                //Envia los datos mediante la petición
                req.tarea = tareaExiste.id
                req.data = dataTarea
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : error})  
        }
    }
}

//Valida los datos para completar una tarea
export const validarCompletar = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarCompletar"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera la id en la petición
            const id = req.params.id

            //Existe la tarea
            const existeTarea = await Tarea.findOne({
                where : {
                    id : id
                }
            })

            //Si existe
            if(existeTarea != null) {

                //Envia los datos en la petición
                req.tarea = existeTarea
                next()

            } else {
                return res.status(404).json({Error : "Esta tarea no existe."})
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error en el servidor"})  
        }
    }
}

//Valida los datos para cambiar el estado a incompleto de una tarea
export const validarIncompleta = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarIncompleta"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera la id en la petición
            const id = req.params.id

            //Existe la tarea
            const existeTarea = await Tarea.findOne({
                where : {
                    id : id
                }
            })

            //Si existe
            if(existeTarea != null) {
                
                //Envia los datos en la petición
                req.tarea = existeTarea
                next()

            } else {
                return res.status(404).json({Error : "Esta tarea no existe."})
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)
            
            return res.status(500).json({Error : error})  
        }
    }
}

//Valida los datos para eliminar la tarea
export const validarEliminarTarea = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarEliminarTarea"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //¿El token es valido?
            const id = req.params.id

            //¿Existe la tarea?
            const existeTarea = await Tarea.findOne({
                where : {
                    id : id
                }
            })


            //Existe
            if(existeTarea != null) {

                //Envia los datos mediante la petición
                req.tarea = existeTarea.id
                next()

            } else {
                return res.status(404).json({Error : "Esta tarea no existe."})
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : error})  
        }
    }
}