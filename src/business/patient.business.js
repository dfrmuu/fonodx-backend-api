import { registrarLogError } from "../controllers/errorlog.controller.js";
import { Medico } from "../models/Medico.js";
import { Paciente } from "../models/Paciente.js";
import { Usuario } from "../models/Usuario.js";
import { Senal } from '../models/Senal.js'
import { DateTime } from "luxon";
import bcrypt from "bcrypt";
import crypto from 'crypto'

/*
 * Proyecto: FonoDX
 * Capa de negocios: Pacientes
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Validación de operaciones, consultas de existencia, agrupamiento de datos.
 */

const VALID_TYPE = "valido"
const RECURSO = "pacientes"


//Esta es la validación para la creación de un paciente
export const validacionPaciente = async (req,res,next) => {  

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validacionPaciente"   

    //Consulta la validez del token que se envia
    if(req.token_status == VALID_TYPE){   
        try{

            //Se traen los campos que vienen del formulario del front
            const {ID_Usuario, familiar, telefono_familiar, edad,clasificacion_inicial ,   
                  foco_aortico} = req.body

            //¿El médico que esta registrando el paciente existe?, se valida por la ID, esta viene en el token.
            const medicoExiste = await Medico.findOne({ 
                where : {
                    id : req.user.IdMedico 
                },
            })
    
            //No, no existe
            if(medicoExiste == null){

                return res.status(404).json({Error : "El medico seleccionado no esta en la base de datos."})

            //Si, existe.
            }else{  
    
                 //Se arma un objeto con los datos del paciente, igual a los campos en la base de datos.
                const data = { 
                    UserID: ID_Usuario,
                    familiar : familiar,
                    familiar_phone : telefono_familiar,
                    age : edad,
                    initial_classification : clasificacion_inicial,
                    aortic_focus : foco_aortico
                }
    
                //Se envian estos datos por la petición tanto los datos, como el médico encargado.
                req.form_data = data  
                req.medico_encargado = req.user.IdMedico
                next()
    
            }
        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(401).json({Error: "Error al validar los datos en registro de paciente"}) 
        }

    }
}


// Generar un username aleatorio
const generarNombreUsuario = (nombreCompleto) => {
    // Dividir el nombre completo en nombres individuales
    const nombres = nombreCompleto.split(' ');
  
    // Tomar el primer nombre o el segundo según la elección
    const nombreElegido = nombres[0]
  
    // Elimina espacios y convierte a minúsculas
    const nombreSinEspacios = nombreElegido.toLowerCase().replace(/\s/g, '');
  
    // Genera números aleatorios entre 1000 y 9999
    const numerosAleatorios = Math.floor(Math.random() * 9000) + 1000;
  
    // Combinar el nombre elegido y los números aleatorios para crear el nombre de usuario
    const nombreUsuario = `${nombreSinEspacios}_${numerosAleatorios}`;
  
    return nombreUsuario;
}

//Generar contraseña aleatoria
const generarContrasena = (longitud) => {
    const caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%&?';
    let contrasena = '';
  
    for (let i = 0; i < longitud; i++) {
      const indice = Math.floor(Math.random() * caracteres.length);
      contrasena += caracteres.charAt(indice);
    }
  
    return contrasena;
}


//Valida la creación simultanea de usuario y paciente
export const validacionUsuarioPaciente = async (req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validacionUsuarioPaciente"  

    //Consulta la validez del token que se envia
    if(req.token_status == VALID_TYPE){
        try{

            //Toma los datos del formulario del front (DATOS USUARIO)
            const {ci, correo,nombre, apellido,direccion,ciudad, celular, rol, politica_aceptada} = req.body 

             //Toma los datos del formulario del front (DATOS PACIENTE)
            const {edad, telefono_familiar, familiar, clasificacion_inicial,foco_aortico} = req.body 

            //Consulta la existencia del usuario a partir del correo y documento
            const usuarioExiste = await Usuario.findOne({ 
                where : {
                    email : correo,
                    ci : ci
                },
            })

            //No, no existe.
            if(usuarioExiste == null){ 

                     // Genera una contraseña aleatoria              
                    const contraseñaRandom = generarContrasena(8);
                    // Asigna un nombre de usuario   
                    const nombreUsuario = generarNombreUsuario(nombre + " " + apellido) 

                    //Construye un objeto con los datos del usuario
                    const data = {   
                        name : nombre,
                        ci : ci,
                        last_name : apellido,
                        address : direccion,
                        city : ciudad,
                        email : correo,
                        phone : celular,
                        name_user : nombreUsuario,
                        password : await bcrypt.hash(contraseñaRandom, 10),
                        RoleID : rol,
                        status : "activo",
                        connected : false,
                        accepted_policy : politica_aceptada,
                    }
                    
                    
                    //Construye un objeto con los datos del paciente
                    const pacienteData = { 
                        age : edad,
                        initial_classification : clasificacion_inicial,
                        aortic_focus : foco_aortico,
                        familiar : familiar,
                        familiar_phone : telefono_familiar,
                    }
    
                    //Envia por petición los datos del usuario
                    req.user_data = data 

                    //Envia por petición los datos del médico
                    req.medico_encargado = req.user.IdMedico  

                    //Envia por petición los datos del paciente
                    req.paciente_data = pacienteData 

                    //Envia por petición la contraseña generada
                    req.emailPass = contraseñaRandom 
                    next()

            }else{ //Si, existe.
                return res.status(403).json({Error : "Este usuario ya existe en la base de datos"})
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(401).json({Error: "Error al validar los datos en registro de paciente"}) 

        }

    }
}

//Valida la consulta de los pacientes a partir de la ID del médico
export const validacionMisPacientes = async(req,res,next) => {

     //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validacionMisPacientes" 

    //Revisa si el token es valido.
    if(req.token_status == VALID_TYPE){ 
        try{

            //Consulta si el médico que llega a través del token, existe.
            const consultaMedico = Medico.findOne({
                where : {
                    id : req.user.IdMedico
                } 
            })

            //No, no existe.
            if(consultaMedico == null){ 
                return res.status(404).json({Error : "El medico que envia, al parecer no existe."})
            }else{  //Si, existe
                req.medico = req.user.IdMedico
                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error :"Ocurrio un error al validar sus pacientes"})
        }
    }
}


//Valida los datos para realizar filtros en la tabla de pacientes
export const validacionFiltrarPacientes = async(req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validacionFiltrarPacientes"

    //Revisa si el token es valido.
    if(req.token_status == VALID_TYPE){
        try{
            
            //Trae los datos del formulario
            const {dato, criterio} = req.body 

            //Envia los datos a través de la petición
            req.dato = dato 
            req.criterio = criterio

            //Consulta si el médico que realiza estos filtros existe.
            const consultaMedico = Medico.findOne({
                where : {
                    id : req.user.IdMedico
                } 
            })

            //No, no existe.
            if(consultaMedico == null){ 
                return res.status(404).json({Error : "El medico que envia, al parecer no existe."})
            }else{ //Si, existe
                req.medico = req.user.IdMedico
                next()
            }


        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error :"Ocurrio un error al validar sus pacientes"})
        }
    }
}

//Valida los datos para consultar un paciente (correo, documento)
export const validacionBusquedaPaciente = async (req, res, next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validacionBusquedaPaciente"

    // Verifica si el token es válido antes de continuar
    if (req.token_status == VALID_TYPE) {

        try {
            
            // Se extrae el criterio de búsqueda y el dato de la solicitud
            const { criterio, dato } = req.body;

            // Verifica si el criterio es "documento" o "correo"
            const isDocumento = criterio === 'documento';
            const isCorreo = criterio === 'correo';

            // Comprueba si el criterio es válido (documento o correo)
            if (!isDocumento && !isCorreo) {
                return res.status(400).json({ Error: `El criterio de búsqueda no es válido.`, Status: 'invalida' });
            }

            // Realiza la consulta para buscar al paciente
            const consultaPaciente = await Usuario.findOne({
                include: [{
                    model: Paciente
                }],
                where: {
                    [isDocumento ? 'ci' : 'email']: dato, // Selecciona la propiedad adecuada según el criterio
                    RoleID: 1
                },
            });

            // Comprueba si el paciente no existe en la base de datos
            if (consultaPaciente === null) {
                return res.status(404).json({
                    Error: `El paciente con el ${isDocumento ? 'documento' : 'correo'} "${dato}" no se encuentra en la base de datos (pruebe realizando la búsqueda por ${isDocumento ? 'correo' : 'documento'}).`,
                    Status: 'inexistente'
                });
            }

            // Extrae los datos del paciente y verifica si el paciente ha completado sus datos
            const { name, phone, last_name, patient } = consultaPaciente;
            if (!consultaPaciente.patient) {
                return res.status(404).json({ Error: `El usuario tiene rol de paciente, pero aún no ha completado sus datos`, Status: 'incompleto', usuario: consultaPaciente.id });
            }

            // Construye el objeto con los datos del paciente
            const datosPaciente = {
                PacientID: patient.id,
                nombre_completo: `${name} ${last_name}`,
                celular: phone,
                edad: patient.age,
                clasificacion_inicial: patient.initial_classification,
                foco_aortico: patient.aortic_focus,
                familiar: patient.familiar,
                createdAt: patient.createdAt,
                familiar_phone: patient.familiar_phone,
                status: "completo"
            };

            // Almacena los datos del paciente en la solicitud y continúa
            req.patient_data = datosPaciente;
            next();


        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error :"Ocurrio un error al buscar el paciente"})
        }

    }
};

//Valida los datos para traer los datos completos de un paciente
export const validarTraerPaciente = async (req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarTraerPaciente" 

    //Revisa si el token es valido
    if(req.token_status == VALID_TYPE){
        try {
            
            //Trae la id que viene en la petición
            const id = req.params.id
 
             //Consulta el paciente
            const consultaPaciente = await Paciente.findOne({
                where : {
                    id : id
                }
            })

            //No, no existe el paciente
            if(consultaPaciente == null){
                return res.status(200).json({Error : "Este paciente no existe"})
            }else { //Si, existe el paciente
                req.paciente = consultaPaciente
                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"}) 
        }
    }
}

//Valida los datos para editar un paciente
export const validarEditarPaciente = async(req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarEditarPaciente"

    //Revisa si el token es valido
    if(req.token_status == VALID_TYPE){
        try {
            
            //Extrae los datos que se envian en el formulario
            const {id,age,initial_classification,aortic_focus,familiar, familiar_phone} = req.body 

            //Consulta el paciente
            const consultaPaciente = await Paciente.findOne({
                where : {
                    id : id
                }
            })

            //No, el paciente no existe.
            if(consultaPaciente == null){
                return res.status(404).json({Error : "Este paciente no existe"})
            } else {

                 //Si, el paciente existe. Crea un objeto con los datos del paciente
                const data = {
                    age : age,
                    initial_classification : initial_classification,
                    aortic_focus : aortic_focus,
                    familiar : familiar,
                    familiar_phone : familiar_phone
                }

                //Envia los datos del paciente y el usuario consultado
                req.data = data
                req.usuario = consultaPaciente.id
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"}) 
        }
    }
} 


//Valida los datos para enviar los resultados del paciente por correo
export const validarEnviarResultados = async(req,res,next) => {

    //Esta variable es para el registro / seguimiento de errores en el sistema mediante la tabla "error_logs"
    const ACCION = "validarEnviarResultados"

    //Revisa si el token es valido
    if(req.token_status == VALID_TYPE){
        try {
            
            //Extrae los datos que se envian en el formulario
            const {PatientID,SignalID} = req.body 

            //Consulta el paciente
            const consultaPaciente = await Usuario.findOne({
                where : {
                    id : PatientID
                }
            })

            //Consulta la auscultación
            const consultaAuscultacion = await Senal.findOne({
                where : {
                    id : SignalID
                }
            })

            //No, el paciente o la auscultación no existe.
            if(consultaPaciente == null || consultaAuscultacion == null){
                return res.status(404).json({Error : "Este paciente / auscultación no existe"})
            } else {

                req.auscultacion = consultaAuscultacion.id
                req.correo = consultaPaciente.email
                next()

            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"}) 
        }
    }
} 