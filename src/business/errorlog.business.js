import { registrarLogError } from '../controllers/errorlog.controller.js'
import { ErrorLog } from '../models/ErrorLog.js'
import { Usuario } from '../models/Usuario.js'

/*
 * Proyecto: FonoDX
 * Capa de negocio: Log de errores
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Validación / agrupamiento de datos.
 */


const VALID_TYPE = "valido"
const RECURSO = "logErrores"

//Valida los datos para recuperar todos los logs de error
export const validarTraerLogsErrorGeneral = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerLogsErrorGeneral"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {
            next()
        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)


            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}

//Valida los datos para recuperar una lista de recursos y errores, para filtros
export const validarTraerRecursos = async (req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerRecursos"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            next()

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }  
}


//Valida los datos para filtrar los logs
export const validarFiltrarDatos = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarFiltrarDatos"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            const {criterio,dato} = req.body

            req.criterio = criterio
            req.dato = dato

            next()

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error :"Ocurrio un error al recuperar los registros de error"})
        }
    }
}