import { registrarLogError } from "../controllers/errorlog.controller.js";
import { Resultado } from "../models/Resultado.js";
import { Senal } from "../models/Senal.js";

/*
 * Proyecto: FonoDX
 * Capa de negocio: Resultados
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Validación / agrupamiento de datos.
 */


const VALID_TYPE = "valido"
const RECURSO = "resultados"

//Valida los datos para recuperar los resultados de una auscultación
export const validarResultadosSenal = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarResultadosSenal"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {
        
            //Recupera el dato en la petición
            const id = req.params.id
    
            //¿Existe la auscultación?
            const consultaSenal = await Senal.findOne({
                where : {
                    id : id
                }
            })

            //No existe
            if(consultaSenal == null){
                return res.status(404).json({Error : "Esta señal no existe."})
            }else{
                //Envia los datos mediante la petición
                req.senal = id
                next()
            }
    
        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}