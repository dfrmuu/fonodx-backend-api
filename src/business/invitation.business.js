import { Invitacion } from "../models/Invitacion.js"
import { Usuario } from "../models/Usuario.js"
import { Administrativo } from "../models/Administrativo.js"
import { Medico } from "../models/Medico.js"
import { DateTime } from "luxon"
import { registrarLogError } from "../controllers/errorlog.controller.js"
import { Op } from "sequelize"


/*
 * Proyecto: FonoDX
 * Capa de negocio: Invitaciones
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Validación / agrupamiento de datos.
 */


const VALID_TYPE = "valido"
const RECURSO = "invitaciones"

//Valida los datos para recuperar las invitaciones de un administrador
export const validacionMisInvitaciones = async (req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validacionMisInvitaciones"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try{

            //Recupera el administrador
            const consultaAdministrador = await Administrativo.findOne({
                where : {
                    id : req.user.IdAdmin
                }
            })

            //¿No existe?
            if(consultaAdministrador == null){
                return res.status(404).json({Error : "El administrador no existe."})
            }else{
                //Envia los datos mediante la petición
                req.admin = req.user.IdAdmin
                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}

//Valida los datos para recuperar la invitación
export const validarTraerInvitacion = async (req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerInvitacion"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {
            
            //Recuperan la id de la petición
            const id = req.params.id

            //¿Existe la invitación?
            const invitacionExistente = await Invitacion.findOne({
                where : {
                    id : id
                }
            })

            //No existe
            if(invitacionExistente == null) {
                return res.status(403).json({Error : "Esta invitación no existe."}) 
            } else {

                //Envia los datos mediante la petición
                req.invitacion = invitacionExistente
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}

//Valida los datos para crear la invitación
export const validacionInvitacion = async (req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validacionInvitacion"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try{

            //Recupera los datos en la petición
            const {email,user_type} = req.body

            //Recupera el administrador
            const consultaAdministrador = await Administrativo.findOne({
                where : {
                    id : req.user.IdAdmin
                }
            })

            //Recupera la invitación
            const invitacionPendiente = await Invitacion.findOne({
                where : {
                    email : email,
                    status : "enviada"
                }
            })

            //Recupera el usuario
            const consultaUsuario = await Usuario.findOne({
                where : {
                    email : email
                }
            })


            if(consultaAdministrador == null){
                return res.status(404).json({Error : "El administrador no existe."})
            }if(consultaUsuario != null){
                return res.status(403).json({Error : "Esta persona ya se encuentra registrada en el sistema."})       
            }if(invitacionPendiente != null){
                return res.status(403).json({Error : "Esta persona ya tiene una invitación pendiente en el sistema."})    
            }else{

                //Trae la fecha actual
                const createdAt = DateTime.local().setZone('UTC').setZone('America/Bogota')

                //Crea la fecha de expiración, agregandole una semana a la actual
                const expirationDate = DateTime.fromISO(createdAt).plus({week : 1}).setZone('UTC').setZone('America/Bogota').toFormat('yyyy-MM-dd HH:mm')

                //Construye el objeto para crear una invitación
                const data = {
                    email : email,
                    user_type : user_type,
                    expirationDate : expirationDate,
                    AdminID : req.user.IdAdmin,
                    status : "enviada",
                    createdAt : createdAt
                }

                //Envia los datos mediante la petición
                req.form_data = data

                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }   
}

//Valida los datos para editar la invitación
export const validacionEditarInvitacion = async (req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validacionEditarInvitacion"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try{

            //Recupera los datos en la petición
            const {id,email,user_type} = req.body

            //Recupera el administrador
            const consultaAdministrador = await Administrativo.findOne({
                where : {
                    id : req.user.IdAdmin
                }
            })

            //Recupera la invitación
            const invitacionExistente = await Invitacion.findOne({
                where : {
                    id : id
                }
            })


            if(consultaAdministrador == null){
                return res.status(404).json({Error : "El administrador no existe."})
            }if(invitacionExistente == null){
                return res.status(403).json({Error : "Esta invitación no existe."})    
            }else{

                //Recupera datos de la invitación consultada
                const emailActual = invitacionExistente.email
                const rolActual = invitacionExistente.user_type
                var rolActualizado = false

                //Construye objeto para editar la invitación
                var data = {
                    user_type : user_type
                }

                //¿El email actual es el mismo que esta presente en la invitación?
                if(emailActual != email){
                    //Agrega el nuevo email al objeto para la edición
                    data.email = email
                }

                //¿El rol actual es el mismo presente en la invitación?
                if(rolActual != user_type){
                    //Cambia rolActualizado a true
                    rolActualizado = true
                }


                //Envia datos mediante la petición
                req.data= data
                req.rolActualizado = rolActualizado
                req.invitacion = invitacionExistente

                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }   
}

//Valida los datos para eliminar la invitación
export const validarEliminarInvitacion = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarEliminarInvitacion"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try{

            //Recupera la id en la petición
            const id = req.params.id

            //Consulta la invitación
            const consultaInvitacion = await Invitacion.findOne({
                where : {
                    id : id
                }
            })

            //No existe
            if(consultaInvitacion == null){
                return res.status(404).json({Error : "Esta invitación no existe."})
            }else{

                //Envia los datos mediante la petición
                req.invitacion = consultaInvitacion.id
                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}

//Valida los datos para eliminar la invitación
export const validarFiltrarDatos = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarFiltrarDatos"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try{

            const {criterio,dato} = req.body

            req.criterio = criterio
            req.data = dato
            next()

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error: "Ocurrio un error en el servidor"})
        }
    }
}