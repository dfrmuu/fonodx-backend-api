import { registrarLogError } from "../controllers/errorlog.controller.js"
import { Usuario } from "../models/Usuario.js"

/*
 * Proyecto: FonoDX
 * Capa de negocio: Log de inicios de sesión
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Validación / agrupamiento de datos.
 */


const VALID_TYPE = "valido"
const RECURSO = "logLogin"

//Valida los datos para traer los logs de inicio de sesión
export const validarTraerLoginLogs = async (req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerLoginLogs"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recuperan la id de la petición
            const id = req.params.id

            //Consulta la existencia del usuario
            const consultaUsuario = await Usuario.findOne({
                where : {
                    id : id
                }
            })

            //¿Existe?
            if(consultaUsuario != null){

                //Envia el usuario en la petición
                req.usuario = consultaUsuario.id
                next()

            }else{
                return res.status(404).json({Error: "El usuario no existe"})
            }


        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error :"Ocurrio un error al recuperar los registros de inicio de sesión"})
        }
    }
}


//Valida los datos para filtrar los logs de inicio de sesión
export const validarFiltrarLoginLogs = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarFiltrarLoginLogs"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos en la petición
            const {UserID,fecha_inicio,fecha_fin} = req.body.data

            //Consulta la existencia de un usuario
            const consultaUsuario = await Usuario.findOne({
                where : {
                    id : UserID
                }
            })

            //¿Se envia la fecha de fin (rango de fechas)?
            if(fecha_fin != '' || fecha_fin != null){
                req.fecha_fin = fecha_fin
            }


            //El usuario existe
            if(consultaUsuario != null){

                //Envia los datos en la petición
                req.usuario = consultaUsuario.id
                req.fecha_inicio = fecha_inicio
                next()
            }else{
                return res.status(500).json({Error :"Este usuario no existe"})
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error :"Ocurrio un error al recuperar los registros de inicio de sesión"})
        }
    }
}