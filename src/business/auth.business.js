import { Usuario } from "../models/Usuario.js";
import { Medico } from "../models/Medico.js";
import { Administrativo } from '../models/Administrativo.js'
import { Permiso } from '../models/Permiso.js'
import { TokenContrasena } from "../models/TokenContrasena.js";
import { DateTime } from "luxon";
import bcrypt from "bcrypt";
import { registrarLogError } from "../controllers/errorlog.controller.js";
import { Invitacion } from "../models/Invitacion.js";
import { Op } from "sequelize";
import { Investigador } from "../models/Investigador.js";
import { Paciente } from "../models/Paciente.js";

const VALID_TYPE = "valido"
const RECURSO = "autentificación"

/*
 * Proyecto: FonoDX
 * Capa de negocio: Autentificación
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 10 - 31
 * Descripción: Validación y agrupamiento de datos para autentificación.
 */

// Función para obtener los permisos de un usuario según su RoleID
async function obtenerPermisosPorRol(RoleID) {
    try {
        const permisos = await Permiso.findAll({
            where: { RoleID : RoleID },
        });
    
        return permisos.map((permiso) => ({ resource: permiso.resource, action: permiso.action }));
    } catch (error) {
        return res.status(500).json({Error : "Ocurrio un error en el servidor"})
    }
}


// Función para obtener información adicional según el rol
async function obtenerInformacionPorRol(userID, RoleID) {


    const datosRol = {
        permisos: await obtenerPermisosPorRol(RoleID),
    };

    if(RoleID == 1) {
        const pacienteConsultado = await Paciente.findOne({ where: { UserID: userID } });
        if (pacienteConsultado != null) {
            datosRol.info = {
                idPaciente: pacienteConsultado.id,
                familiar: pacienteConsultado.familiar,
                telefonoFamiliar: pacienteConsultado.familiar_phone,
            };
        }

    } else if (RoleID === 2) { // Medico
        const medicoConsultado = await Medico.findOne({ where: { UserID: userID } });
        if (medicoConsultado != null) {
            datosRol.info = {
                idMedico: medicoConsultado.id,
                cargo: medicoConsultado.job_title,
                area: medicoConsultado.area,
            };
        }
    } else if (RoleID === 3) {
        const investigadorConsultado = await Investigador.findOne({where : { UserID : userID}});
        if (investigadorConsultado != null) {
            datosRol.info = {
                idInvestigador: investigadorConsultado.id,
                cargo: investigadorConsultado.job_title,
                area: investigadorConsultado.area,
            };
        }
    } else if (RoleID === 4) { // Administrador
        const administrativoConsultado = await Administrativo.findOne({ where: { UserID: userID } });
        if (administrativoConsultado != null) {
            datosRol.info = {
                idAdmin: administrativoConsultado.id,
                cargo: administrativoConsultado.job_title,
                area: administrativoConsultado.area,
            };
        }
    }

    return datosRol;
}

// Validar los datos para el inicio de sesión
export const validarLogin = async (req, res, next) => {

    // Revisa si el token es válido
     if (req.token_status == VALID_TYPE){

        try {
            // Recupera los datos del formulario
            const { email, password, device } = req.body;

            // Busca si el usuario existe a partir del correo
            const user = await Usuario.findOne({
                where: {
                    email : email
                },
            });

            // ¿Existe?
            if (user === null) {
                return res.status(401).json({ Error: 'Datos incorrectos' });
            }

            // Obtiene la información del usuario basada en su rol
            const datosRol = await obtenerInformacionPorRol(user.id, user.RoleID);


            req.user_data = user // Datos del usuario
            req.role_info = datosRol
            req.permisos = datosRol.permisos// Permisos del usuario
            req.user_login = { password } // Contraseña para iniciar sesión
            req.device = device // Desde qué dispositivo inició sesión

            next();
        } catch (error) {
            console.log(error)
            return res.status(401).json({ Error: 'Error al validar los datos de inicio de sesión' });
        }
    }
}

//Genera un nombre aleatorio a partir del nombre completo de un usuario
const generarNombreUsuario = (nombreCompleto) => {
    // Dividir el nombre completo en nombres individuales
    const nombres = nombreCompleto.split(' ');
  
    // Tomar el primer nombre o el segundo según la elección
    const nombreElegido = nombres[0]
  
    // Elimina espacios y convierte a minúsculas
    const nombreSinEspacios = nombreElegido.toLowerCase().replace(/\s/g, '');
  
    // Genera números aleatorios entre 1000 y 9999
    const numerosAleatorios = Math.floor(Math.random() * 9000) + 1000;
  
    // Combinar el nombre elegido y los números aleatorios para crear el nombre de usuario
    const nombreUsuario = `${nombreSinEspacios}_${numerosAleatorios}`;
  
    return nombreUsuario;
}


//Valida los datos de registro para un nuevo usuario
export const validarRegistro = async(req,res,next) => {

    // Revisa si el token es válido (o la clave de cliente)
    if(req.token_status == VALID_TYPE){
        try {
        
            //Trae los datos que se envian en el cuerpo de la petición. 
            const {invitacion,ci, name, last_name, email, phone, city, address, password,roleID, job_title, area } = req.body


            //Revisa si la invitación es valida y existe.
            const existeInvitacion = await Invitacion.findOne({
                where : {
                    id : invitacion,
                    status : 'enviada'
                }
            })

            //No existe o ya expiró.
            if(existeInvitacion == null){
                return res.status(400).json({ Error : "Esta invitación no existe, o ya expiró."})
            } else {

                //¿Existe el usuario?
                const existenciaUsuario = await Usuario.findOne({
                    where : {
                        [Op.or] : [
                            {email : email},
                            {ci : ci}
                        ]
                    }
                })

                //No existe.
                if(existenciaUsuario != null){

                    return res.status(400).json({ Error : "Se encontró un usuario con el mismo DOCUMENTO o CORREO, rectifique sus datos"})

                } else {

                    //Construye los datos para registrarlos después (usuario)
                    const data = {
                        ci : ci,
                        name : name, 
                        last_name : last_name,
                        email : email,
                        phone : phone,
                        city : city,
                        address : address,
                        status : "activo",
                        RoleID : roleID,
                        password : await bcrypt.hash(password,10),
                        name_user : generarNombreUsuario(name + " " + last_name),
                        accepted_policy : true,
                    }

                    //Construye los datos para registrarlos después (rol)
                    const jobdata = {
                        job_title : job_title,
                        area : area
                    }

                    //Envia los datos mediante la petición
                    req.data = data
                    req.jobdata = jobdata
                    req.invitacion = existeInvitacion
                    next()

                }
            }

        }catch(error){

            return res.status(500).json({ Error: 'Error al generar al validar sus datos de registro' });

        }
    }
}

//Valida las invitaciones que se han enviado a un correo
export const validarCorreo = async (req,res,next) => {
    try{

        //Trae correo
        const {correo} = req.body 

        //Consulta las invitaciones a partir de un correo y el estado "enviada"
        const consultaCorreo = await Invitacion.findOne({
            where : {
                email : correo,
                status : "enviada"
            }
        })

        //¿Tiene invitaciones?
        if(consultaCorreo == null){
            return res.status(409).json({Error : "Al parecer no tiene invitaciones pendientes."})
        }else{

            //Extrae los datos de la invitacion
            const {email,user_type,id} = consultaCorreo

            const data = {
                email : email,
                user_type : user_type,
                id : id
            }

            req.data = data
            next()

        }
    }catch(error){
        return res.status(500).json({Error: "Ocurrio un error al consultar."}) 
    }
}

//Valida los datos para los tokens de recuperación de contraseña
export const validarGenerarTokenRecuperacion = async(req,res,next) => {

    const ACCION = 'validarGenerarTokenRecuperacion';

    // Revisa si el token es válido
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos del formulario
            const {email} = req.body

            //Consulta un usuario a partir del correo
            const consultaCorreo = await Usuario.findOne({
                where : {
                    email : email
                }
            })

            //¿Existe?
            if(consultaCorreo == null){
                return res.status(404).json({Error: "Este usuario no existe."})
            } else {
                req.usuario = consultaCorreo
                next()
            }

                

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({ Error: 'Error al generar el token de recuperación' });

        }
    }
}

//Valida el token de recuperación
export const validarToken = async (req,res,next) => {

    const ACCION = 'validarToken';

    // Revisa si el token es válido
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos del formulario
            const {token} = req.body

            //Consulta el token
            const existeToken = await TokenContrasena.findOne({
                where : {
                    token : token
                }
            })

            //¿Existe?
            if(existeToken == null){
                return res.status(404).json({Error: "Este token no existe."})
            }else{

                const fechaCreacion = DateTime.fromISO(existeToken.createdAt).setZone('America/Bogota')
                const fechaExpiracion = DateTime.fromISO(existeToken.expirationDate).setZone('America/Bogota')

                // Comparar si la fecha de expiración es anterior a la fecha de creación del token.
                if (fechaExpiracion < fechaCreacion) {
                    return res.status(200).json({Error : "Token expirado, solicite uno nuevo."})
                } else {
                    req.existe = true
                    next()
                }
            }

            
        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(401).json({Error: "Error al validar los datos de registro"}) 
        }
    }
}


//Valida la recuperación de credencial (contraseña)
export const validarRecuperarCredenciales = async(req,res,next) => {

    const ACCION = 'validarRecuperarCredenciales';

    // Revisa si el token es válido
    if(req.token_status == VALID_TYPE){
        try {

             //Recupera los datos del formulario
            const {clave,email} = req.body

            //Revisa que el email no venga vacio
            if(email != "" || email != ""){

                //Consulta el usuario por email
                const consultaUsuario = await Usuario.findOne({
                    where : {
                        email : email
                    }
                })

                //¿Existe?
                if(consultaUsuario == null){
                    return res.status(404).json({Error: "Este usuario no existe."})
                } else {
    
                    req.usuario = consultaUsuario
                    req.clave = clave
                    next()
                }
    

            } else {

                return res.status(400).json({Error : "Ocurrio un error al validar los datos, intentelo más tarde."})

            }


        } catch (error ){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(400).json({Error : "Ocurrio un error al recuperar la clave, intentelo más tarde."})
        }
    }
}
