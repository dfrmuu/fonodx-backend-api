import { Usuario } from "../models/Usuario.js";
import { Medico } from "../models/Medico.js";
import { Administrativo } from '../models/Administrativo.js'
import bcrypt from "bcrypt";
import { registrarLogError } from "../controllers/errorlog.controller.js";

/*
 * Proyecto: FonoDX
 * Capa de negocio: Actividades
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Validación / agrupamiento de datos.
 */


const VALID_TYPE = "valido"
const RECURSO = "actividades"


//Valida los datos para recuperar las actividades
export const validacionActividades = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validacionActividades"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos en la petición
            const ID = req.user.Id
            
            //¿Existe el usuario?
            const usuarioExiste = await Usuario.findOne({
                where : {
                    id : ID
                }
            })

            //No existe
            if(usuarioExiste == null){
                return res.status(404).json({Error: "Usuario no encontrado"}) 
            }else{

                //Envia los datos mediante la petición
                req.usuario = usuarioExiste.id
                next()
            }


        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : error})  
        }
    }
}

//Valida los datos para la creación de una actividad
export const validacionCrearActividad = async (req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validacionCrearActividad"


    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Trae los datos en la petición
            const {actividad} = req.body
        
            //¿Existe el usuario?
            const usuarioExiste = await Usuario.findOne({ 
                where : {
                    id : req.user.Id
                }
            })

            //No existe
            if(usuarioExiste == null){
                return res.status(404).json({Error: "Usuario no encontrado"})
            }else{

                //Construye el objeto para registrar la actividad
                const data = {
                    UserID : usuarioExiste.id,
                    activity : actividad,
                }

                //Pasa los datos de la actividad mediante la petición
                req.form_data = data 
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : error})
        }
    }
}