import { DateTime } from "luxon";
import { Rol } from "../models/Rol.js"
import { Usuario } from "../models/Usuario.js"
import bcrypt from "bcrypt";
import { Paciente } from "../models/Paciente.js";
import { Medico } from "../models/Medico.js";
import { Administrativo } from "../models/Administrativo.js";
import { sequelize } from "../database/database.js";
import { Investigador } from "../models/Investigador.js";
import { registrarLogError } from "../controllers/errorlog.controller.js";

/*
 * Proyecto: FonoDX
 * Capa de negocio: Usuarios
 * Desarrollado por: Fundación Abood Shaio
 * Ultima fecha de cambios: 2023 - 11 - 02
 * Descripción: Validación / agrupamiento de datos.
 */


const VALID_TYPE = "valido"
const RECURSO = "usuarios"

//Generar un nombre de usuario aleatorio, de acuerdo al nombre completo
const generarNombreUsuario = (nombreCompleto) => {
    // Dividir el nombre completo en nombres individuales
    const nombres = nombreCompleto.split(' ');
  
    // Tomar el primer nombre o el segundo según la elección
    const nombreElegido = nombres[0]
  
    // Elimina espacios y convierte a minúsculas
    const nombreSinEspacios = nombreElegido.toLowerCase().replace(/\s/g, '');
  
    // Genera números aleatorios entre 1000 y 9999
    const numerosAleatorios = Math.floor(Math.random() * 9000) + 1000;
  
    // Combinar el nombre elegido y los números aleatorios para crear el nombre de usuario
    const nombreUsuario = `${nombreSinEspacios}_${numerosAleatorios}`;
  
    return nombreUsuario;
}

//Validar datos para recuperar usuarios de la base de datos
export const validarTraerUsuarios = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerUsuarios"

    //¿Es valido el token?
    if(req.token_status == VALID_TYPE){
        try {
            next()
        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida datos para recuperar el usuario que haya iniciado sesión
export const validarTraerMiUsuario = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarTraerMiUsuario"

    //¿Es valido el token?
    if(req.token_status == VALID_TYPE){
        try {

            //Consulta la existencia del usuario
            const usuario = await Usuario.findOne({
                where : {
                    id : req.user.Id
                },
                attributes : ['id','ci','name','last_name','email','phone','city','address']
            })

            //No existe
            if(usuario == null){
                return res.status(404).json({Error: "Este usuario no existe."})
            } else {

                //Envia los datos en la petición
                req.usuario = usuario
                next()
            }

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida datos para la edición de un usuario
export const validarEditarMiUsuario = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarEditarMiUsuario"

    if(req.token_status == VALID_TYPE){
        try {
            
            //Recupera los datos en la petición
            const {ci,email,phone,names,last_name,city,address} = req.body

            //Consulta la existencia del usuario
            const usuario = await Usuario.findOne({
                where : {
                    id : req.user.Id
                }
            })

            //No existe
            if(usuario == null){
                return res.status(404).json({Error: "Este usuario no existe."})
            } else {

                //Construye el objeto para la edición
                const data = {
                    ci : ci,
                    email : email,
                    phone : phone,
                    name : names, 
                    last_name : last_name,
                    city : city,
                    address : address
                }

                //Envia los datos por la petición
                req.data = data
                req.usuario = usuario.id
                next()
            }


        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}


//Valida los datos para recuperar un usuario
export const validarTraerUsuario = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarEditarMiUsuario"

    //¿Token valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos en la petición
            const id = req.params.id

            //Consulta la existencia del usuario
            const usuario = await Usuario.findOne({
                where : {
                    id :id
                },
                attributes : ['id','ci','RoleID','name','last_name','email','phone','city','address']
            })

            //No existe
            if(usuario == null){
                return res.status(404).json({Error: "Este usuario no existe."})
            } else {
                //Según el rol
                switch (usuario.RoleID) {
                    case 1 :

                        //Recupera los datos del paciente
                        const paciente = await Paciente.findOne({
                            where : {
                                UserID : usuario.id
                            },
                            attributes : ['age','familiar','familiar_phone']
                        })

                        //Envia los datos del usuario, más los del paciente
                        usuario.dataValues.age = paciente.age
                        usuario.dataValues.familiar = paciente.familiar
                        usuario.dataValues.familiar_phone = paciente.familiar_phone
                    break;
                    case 2:

                        //Recupera los datos del médico
                        const medico = await Medico.findOne({
                            where : {
                                UserID : usuario.id
                            },
                            attributes : ['job_title','area']
                        })

                        //Envia los datos del usuario, más los del médico
                        usuario.dataValues.job_title = medico.job_title
                        usuario.dataValues.area = medico.area
                    break;
                    case 3:

                        //Recupera los datos del investigador
                        const investigador = await Investigador.findOne({
                            where : {
                                UserID : usuario.id
                            },
                            attributes : ['job_title','area']
                        })

                        
                        //Envia los datos del usuario, más los del investigador
                        usuario.dataValues.job_title = investigador.job_title
                        usuario.dataValues.area = investigador.area
                    break;
                    case 4:

                        //Recupera los datos del administrador
                        const admin = await Administrativo.findOne({
                            where : {
                                UserID : usuario.id
                            },
                            attributes : ['job_title','area']
                        })
                        
                        //Envia los datos del usuario, más los del administrador
                        usuario.dataValues.job_title = admin.job_title
                        usuario.dataValues.area = admin.area
                    break;
                }
                
                //Envia los datos mediante la petición
                req.usuario = usuario
                next()
            }
        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida los datos para desactivar un usuario
export const validarDesactivarUsuario = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarDesactivarUsuario"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Trae la id en la petición
            const id = req.params.id

            //Consulta el usuario por id
            const usuario = await Usuario.findOne({
                where : {
                    id :id
                }
            })

            //No existe
            if(usuario == null){
                return res.status(404).json({ Error : "Este usuario no existe."})
            }else{

                //Envia el usuario mediante la petición
                req.usuario = usuario
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida los datos para activar un usuario
export const validarActivarUsuario = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarActivarUsuario"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

           //Trae la id en la petición
            const id = req.params.id

            //Consulta el usuario por id            
            const usuario = await Usuario.findOne({
                where : {
                    id :id
                }
            })

            //No existe
            if(usuario == null){
                return res.status(404).json({ Error : "Este usuario no existe."})
            }else{

                //Envia el usuario mediante la petición
                req.usuario = usuario
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Crea una contraseña aleatoria
const generarContrasena = (longitud) => {

    //Lista de caracteres para generar la contraseña
    const caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-=[]{}|;:,.<>?';
    let contrasena = '';
  
    //Ciclo para crear una contraseña aleatoria
    for (let i = 0; i < longitud; i++) {
      const indice = Math.floor(Math.random() * caracteres.length);
      contrasena += caracteres.charAt(indice);
    }
  
    return contrasena;
}

//Valida los datos para la creación de un usuario
export const validarCrearUsuario = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarCrearUsuario"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera los datos en la petición
            const {ci,email,name,last_name,phone,city,address,rol,job_title,area} = req.body
            //Genera una contraseña aleatoria
            const contrasenaGenerada = generarContrasena(7)
            //Crea un nombre de usuario aleatorio
            const nombreUsuario = generarNombreUsuario(name + " " + last_name)

            
            //Consulta el usuario por el email
            const usuario = await Usuario.findOne({
                where : {
                    email : email
                }
            })

            //¿Ya existe?
            if(usuario != null){
                return res.status(404).json({ Error : "Este correo ya se encuentra registrado en el sistema."})
            }else{

                //Construye los datos para la creación de un usuario
                const data = {
                    ci : ci,
                    email : email,
                    phone : phone,
                    name : name,
                    last_name : last_name,
                    accepted_policy : true,
                    status : "activo",
                    city : city,
                    address : address,
                    password : await bcrypt.hash(contrasenaGenerada,10),
                    name_user : nombreUsuario,
                }

                //Construye los datos para la asignación al rol
                const dataRol = {
                    job_title : job_title,
                    area : area
                }

                //Según el rol agrega el indicador al objeto de datos de creación
                switch (rol) {
                    case "administrativo":
                        data.RoleID = 4
                    break;
                    case "medico":
                        data.RoleID = 2
                    break;
                    case "paciente":
                        data.RoleID = 1
                    break;
                    case "investigador" : 
                        data.RoleID= 3
                    break;
                }

                //Envia los datos mediante la petición
                req.data = data
                req.dataRol = dataRol
                req.contrasena = contrasenaGenerada
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida los datos para la edición de un usuario
export const validarEditarUsuario = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarEditarUsuario"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {
    
            //Recupera los datos en la petición
            const {id,ci,email,phone,name,last_name,city,address,job_title, area,age,familiar,familiar_phone} = req.body

            //Consulta la existencia del usuario
            const usuario = await Usuario.findOne({
                where : {
                    id : id
                }
            })

            //No existe
            if(usuario == null){
                return res.status(404).json({ Error : "Este usuario no existe."})
            }else{

                var job_data

                //Construye objeto con los datos del usuario para edición
                const data = {
                    ci : ci,
                    email : email,
                    phone : phone,
                    name : name,
                    last_name : last_name,
                    city : city,
                    address : address,
                }


                //Según el rol
                if(usuario.RoleID == 1){
                    //Construye objeto con los datos del rol para edición
                    job_data = { 
                        age : age,
                        familiar : familiar,
                        familiar_phone : familiar_phone
                    }
                } else {
                    //Construye objeto con los datos del rol para edición
                    job_data = {
                        job_title : job_title,
                        area : area
                    }
                }



                req.data = data
                req.usuario = usuario
                req.job_data = job_data
                next()
            }

        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida los datos para eliminar un usuario
export const validarEliminarUsuario = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarEliminarUsuario"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {

            //Recupera la id en la petición
            const id = req.params.id

            //Validación mismo usuario
            if(id == req.user.Id){
                return res.status(400).json({Error : "No puedes eliminar tú propio usuario."})
            }

            //Consulta la existencia del usuario
            const usuario = await Usuario.findOne({
                where : {
                    id : id
                }
            })

            //No existe
            if(usuario == null){
                return res.status(404).json({ Error : "Este usuario no existe."})
            }else{ 

                //Envia los datos mediante la petición
                req.usuario = usuario
                next()
            }

        }catch(error){
             
            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)

            return res.status(500).json({Error : "Ocurrio un error"})
        }
    }
}

//Valida los datos para filtrar datos de usuarios
export const validacionFiltrarUsuarios= async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validacionFiltrarUsuarios"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try{

            //Recupera los datos en la petición
            const {dato, criterio} = req.body


            //Envia los datos en la petición
            req.dato = dato
            req.criterio = criterio

            next()

        }catch(error){

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)


            return res.status(500).json({Error :"Ocurrio un error al validar sus pacientes"})
        }
    }
}

//Valida los datos para la revisión de la clave
export const validarRevisionClave = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
     const ACCION = "validarRevisionClave"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {
            
            //Recupera los datos en la petición
            const {clave} = req.body
            const usuario = req.user.Id

            //Consulta la existencia del usuario
            const existenciaUsuario = await Usuario.findOne({
                where : {
                    id : usuario
                }
            })

            //El usuario no existe
            if(existenciaUsuario == null){
                return res.status(404).json({Error : "Este usuario no existe"})
            }else{

                //Compara la clave enviada con la de la base de datos
                const comparacionContraseña = await bcrypt.compare(clave, existenciaUsuario.password)

                //¿Valida?
                if(comparacionContraseña){
                    req.valido = true
                    next()
                } else {
                    return res.status(400).json({Error : "Las claves no coinciden"})
                }
            }
        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)


            return res.status(500).json({Error :"Ocurrio un error al validar sus pacientes"})
        }
    }
}

//Valida los datos para el cambio de clave
export const validarCambioClave = async(req,res,next) => {

    //Esto es para agregar el origen del error en el log
    const ACCION = "validarCambioClave"

    //¿El token es valido?
    if(req.token_status == VALID_TYPE){
        try {
        
            //Recupera los datos en la petición
            const {clave} = req.body
            const usuario = req.user.Id

            //Consulta la existencia del usuario
            const existenciaUsuario = await Usuario.findOne({
                where : {
                    id : usuario
                }
            })

            //El usuario no existe
            if(existenciaUsuario == null){
                return res.status(500).json({Error : "Este usuario no existe"})
            }else{

                //Envia los datos mediante la petición
                req.clave = clave
                req.usuario = existenciaUsuario
                next()
            }
        } catch (error) {

            //Se arma el objeto para el registro en el log
            const dataError = { 
                resource : RECURSO,
                action : ACCION,
                info : error.message
            }

            const registroError = await registrarLogError(dataError)


            return res.status(500).json({Error :"Ocurrio un error al validar sus pacientes"})
        }
    }
}